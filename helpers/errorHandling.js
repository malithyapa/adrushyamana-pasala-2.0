import firestore from '@react-native-firebase/firestore';
import Toast from 'react-native-simple-toast';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {NetworkInfo} from 'react-native-network-info';
import DeviceInfo from "react-native-device-info";
import store from './store';

export async function handleError(err, caller) {
  console.log('Handle Error: Caller:' + caller);

  const ip = await NetworkInfo.getIPAddress();

  const error = {
    error: err,
    errorString: err.toString(),
    time: new Date().getTime(),
    timeString: new Date().toString(),
    timezoneOffset: -(new Date().getTimezoneOffset()/60),
    caller: caller,
    userEmail: store.getState().email,
    version: store.getState().version,
    publicIP: ip,
    androidVersion: DeviceInfo.getSystemVersion(),
    dev: store.getState().dev
  };
  console.log('Handle Error: Err:\n', error);

  const errors = JSON.parse(await AsyncStorage.getItem('@errors'));
  if(!errors) errors = [];
  errors.push(error)
  await AsyncStorage.setItem('@errors', JSON.stringify(errors));

  if(store.getState().version%100 !== 0){
    Toast.show('Err :' + err.code + ', ' + err.toString(), Toast.LONG);
  }
}
