import React, {useState, useEffect} from 'react';
import { StyleSheet, Text, View, Image, Dimensions, ScrollView, BackHandler, Alert, Pressable} from 'react-native';
import { TITLE, LIBRARY, SEARCH, INFO, HEART, HEART_W, UP, DOWN, RIGHT, SETTINGS } from '../images/index';
import { useSelector, useDispatch } from 'react-redux'
import firestore from '@react-native-firebase/firestore';
import { getUniqueId } from 'react-native-device-info';
import Toast from 'react-native-simple-toast';
import { useFocusEffect } from '@react-navigation/native';
const MAX_CATEGORY_COUNT = 200;

const Categories = ({navigation}) => {

  const [rerender, setRerender] = useState(false);

  useFocusEffect(() => {
    const backAction = () => {
      Alert.alert("", labels[9][language], [
        {
          text: "Cancel",
          onPress: () => null,
          style: "cancel"
        },
        { text: "YES", onPress: () => BackHandler.exitApp() }
      ]);
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      backAction
    );


    return () => backHandler.remove();
  });

  navigation.addListener(
    'didFocus',
    payload => {
     setRerender(!rerender);
    }
  );

  const dispatch = useDispatch();
  const email = useSelector(state => state.email);

  // const [expandAnim, setExpandAnim] = useState(Array(MAX_CATEGORY_COUNT).fill(0));
  // const [expandAnimLS, setExpandAnimLS] = useState(Array(MAX_CATEGORY_COUNT).fill(0));
  // const [expandedFlag, setExpandedFlag] = useState(Array(MAX_CATEGORY_COUNT).fill(false));
  // const [expandedFlagLS, setExpandedFlagLS] = useState(Array(MAX_CATEGORY_COUNT).fill(false));

  const state = useSelector(state => state);
  let favCategories = state.favCategories || [];
  let language = state.language;
  // let stats = state.stats;
  const customMessageShown = state.customMessageShown;
  let categories = state.categories;
  // let favCategories = useSelector(state => state.favCategories) || [];
  // let language = useSelector(state => state.language);
  // let stats = useSelector(state => state.stats);
  // const customMessageShown = useSelector(state => state.customMessageShown);
  // let categories = useSelector((state) => state.categories);

  // if(stats['0_customToast'] && !customMessageShown) {
  //   Toast.show(stats['0_customToastMSG'][language], Toast.LONG);
  //   dispatch({type: 'setCustomMessageShown', values: true});
  // }

  const dimensions = Dimensions.get('window');
  const imageHeight = Math.round(dimensions.width / 2.048);
  const screenWidth = dimensions.width;
  const navButtonWidth = screenWidth/3;
  const buttonWidth = screenWidth*0.96;

  const labels = [
    ['සොයන්න', 'Search', 'தேடல்'],
    ['මගේ පුස්තකාලය', 'Library', 'நூலகம்'],
    ['අපි', 'About', 'பற்றி'],
    ['අපි', 'About', 'பற்றி'],
    ['වීඩියෝ', 'Videos', 'வீடியோக்கள்'],
    ['ලිපි', 'Articles', 'கட்டுரைகள்'],
    ['ප්‍රවර්ගය උඩට යවන ලදීි', 'Category moved to the top', 'வகை மேலே நகர்த்தப்பட்டது'],
    ['දේශීය විෂය නිර්දේශය', 'Local Syllabus', 'உள்ளூர் பாடத்திட்டம்'],
    ['ඔබ කැමති විෂයන් තුල තරු ලකුණ එබීමෙන් පසු ඒවා මෙහි දිස්වේ', 'You can press the star button inside your favourite categories to display them here', 'உங்களுக்குப் பிடித்த வகைகளுக்குள் உள்ள நட்சத்திர பொத்தானை அழுத்தினால் அவற்றை இங்கே காண்பிக்கலாம்'],
    ['යෙදුමෙන් ඉවත් වන්න?', 'Exit the app?', 'பயன்பாட்டிலிருந்து வெளியேறவும்?'],
    ['නව අන්තර්ගතයන්', 'New Content', 'புதிய உள்ளடக்கம்']
  ]

  const searchPressed = () => {
    navigation.navigate('Search', {})
  }

  const aboutPressed = () => {
    navigation.navigate('About', {})
  }

  const libraryPressed = () => {
    navigation.navigate('Library', {})
  }

  const buttonPressed = (index) => {

    if(index != 199 && !categories[index].subCategories.length) {
      navigation.navigate('Content', {sub: categories[index].name});
      return;
    }

    if(!expandedFlag[index]) {
      // expandAnim[index] = 10000;//categories[index].subCategories.length*90;
      // setExpandAnim(Array.from(expandAnim));
      // expandedFlag[index] = true;
    } else {
      // expandAnim[index] = 0;
      // setExpandAnim(Array.from(expandAnim));
      // expandedFlag[index] = false;
    }
  }

  const buttonPressedLS = (index) => {

    if(!categories[0].subCategories[index].subCategories.length) {
      navigation.navigate('Content', {sub: categories[0].subCategories[index].name});
      return;
    }

    if(!expandedFlagLS[index]) {
      // expandAnimLS[index] = categories[0].subCategories[index].subCategories.length*90;
      // setExpandAnimLS(Array.from(expandAnimLS));
      // expandedFlagLS[index] = true;
    } else {
      // expandAnimLS[index] = 0;
      // setExpandAnimLS(Array.from(expandAnimLS));
      // expandedFlagLS[index] = false;
    }
  }

  const getTemplate = (index, category, subCategories, grouped, color1, color2, newContent, placeholder) => {
    const categoryTemplate = [];
    let categoryTitle  = category;
    if(category.search('(LS)') > 0) {
      categoryTitle = category.replace('(LS)', '');
    }
    category = category.trim();

    let newContentInSub = subCategories.find(sub => sub.lastVisited<sub.lastUpdated);

    categoryTemplate.push(
      <View
        key={index}
        style={{...styles.button, backgroundColor: color1, borderRightWidth: grouped ? 10 : 0, borderRightColor: '#2b783d', width: buttonWidth}}>
        <Pressable
          onPress={() => {
            if(grouped) buttonPressedLS(index);
            else buttonPressed(index);
          }}>
          <View style={{...styles.categoryTouchArea, width: buttonWidth}}>
            {(subCategories.length ? newContentInSub : newContent) && (
              <View style={{
                position: 'absolute',
                top: 0,
                right: 0,
                width: 50
              }}>
                <View style={{
                  width: '100%',
                  height: 20,
                  backgroundColor: '#eaaf39'
                }}/>
                <View style={{
                  width: '100%',
                  height: 7,
                  backgroundColor: '#ff5722'
                }}/>
            </View>
            )}
            <View style={styles.categoryButton}>

              <Text style={{...styles.buttonText, width: '80%'}}>{categoryTitle}</Text>
              {((subCategories.length > 0) || (index == 199)) && (
                <Image
                  style={{...styles.expandImage, marginRight: grouped? 25 : 15}}
                  source={
                    grouped
                    ? (expandedFlagLS[index] ? UP : DOWN)
                    : (expandedFlag[index] ? UP : DOWN)}/>
              )}
            </View>
          </View>
        </Pressable>
      </View>
    )

    if(subCategories && subCategories.length) {
      const subCategoryTemplate = [];
      subCategories.map((sub, index2) => {
        const subNewContent = sub.lastUpdated>sub.lastVisited;
        let gradeTemplate = <Text style={styles.buttonText}>{sub.sub}</Text>
        const grade = /Grade (\d+)/g.exec(sub.sub);
        if(grade && (index != 199)) {
          if(language == 0) {
            gradeTemplate = <Text style={styles.buttonText}>{grade[1]} ශ්‍රේණිය</Text>
          }
          if(language == 1) {
            gradeTemplate = <Text style={styles.buttonText}>Grade {grade[1]}</Text>
          }
          if(language == 2) {
            gradeTemplate = <Text style={styles.buttonText}>தரம் {grade[1]}</Text>
          }
        }
        subCategoryTemplate.push((
          <View
            key={index2}
            style={{...styles.button, ...styles.subButton, backgroundColor: color2, borderRightColor: color1, width: buttonWidth}} >
            <Pressable
              onPress={() => {
                navigation.navigate('Content', {sub: sub.name})
              }}>
              {subNewContent && (
                <View style={{
                  position: 'absolute',
                  top: 0,
                  right: 10,
                  width: 50
                }}>
                  <View style={{
                    width: '100%',
                    height: 20,
                    backgroundColor: '#eaaf39'
                  }}/>
                  <View style={{
                    width: '100%',
                    height: 7,
                    backgroundColor: '#ff5722'
                  }}/>
              </View>
              )}
              <View style={{...styles.categoryTouchArea, width: buttonWidth}}>
                {gradeTemplate}
              </View>
            </Pressable>
          </View>
        ))
      })
      categoryTemplate.push(
        <View
          key={'sub' + index}
          style={styles.subParent}>
          <View style={{...styles.subContainer, maxHeight: grouped ? expandAnimLS[index] : expandAnim[index]}}>
            {(subCategories.length == 0) &&
              <View>
                <Text>{placeholder}</Text>
              </View>
            }
            {subCategoryTemplate}
            <View style={{marginBottom: 10}}></View>
          </View>
        </View>
      )
    } else if(placeholder) {
      categoryTemplate.push(
        <View
          key={'sub' + index}
          style={styles.subParent}>
          <View style={{...styles.subContainer, maxHeight: grouped ? expandAnimLS[index] : expandAnim[index]}}>
              <View style={{padding: 12}}>
                <Text style={{color: 'white'}}>{placeholder}</Text>
              </View>
            <View style={{marginBottom: 10}}></View>
          </View>
        </View>
      )
    }
    return categoryTemplate;
  }

  const buttonsTemplate = [];
  buttonsTemplate.push(getTemplate(199, 'ප්‍රියතම විෂයන් (Favourites)', favCategories, false, '#008c7b', '#32549d', false, labels[8][language]));

  categories.map((cat, index1) => {
    if(cat.grouped) {
      let categoryTitle  = cat.name;
      if(cat.name.search('(LS)') > 0) {
        categoryTitle = cat.name.replace('(LS)');
      }
      category = cat.name.trim();
      buttonsTemplate.push(
        <View
          key={index1}
          style={{...styles.button, backgroundColor: '#2b783d', width: buttonWidth}}>
          <Pressable
            onPress={() => buttonPressed(index1)}>
            <View style={{...styles.categoryTouchArea, width: buttonWidth}}>
              <View style={styles.categoryButton}>
                <Text style={{...styles.buttonText, width: '80%'}}>{categoryTitle}</Text>
                <Image
                  style={styles.expandImage}
                  source={expandedFlag[index1] ? UP : DOWN}/>
              </View>
            </View>
          </Pressable>
        </View>
      );

      const groupedButtonsTemplate = [];
      cat.subCategories.map((sub, index) => {
        groupedButtonsTemplate.push(getTemplate(index, sub.name, sub.subCategories, true, '#006e6a', '#32549d', sub.lastUpdated>sub.lastVisited));
      })

      buttonsTemplate.push(
        <View key={'subLS'} style={styles.subParent}>
          <View style={{...styles.subContainer, maxHeight: expandAnim[index1]}}>
            {groupedButtonsTemplate}
            <View style={{marginBottom: 10}}></View>
          </View>
        </View>
      )
    } else {
      buttonsTemplate.push(getTemplate(index1, cat.name, cat.subCategories, false, '#327a60', '#32549d', cat.lastUpdated>cat.lastVisited));
    }
  });

  buttonsTemplate.push((
    <View key={'dummy'} style={{width: '100%', height: 100}}></View>
  ));

  return (
    <View>
      <ScrollView contentContainerStyle={styles.scrollContainer}>
        <Image
            style={{ height: imageHeight, width: screenWidth}}
            source={TITLE}/>
        <View style={{
          position: 'absolute',
          flexDirection: 'row',
          justifyContent: 'flex-start',
          alignItems: 'center',
          top: imageHeight-50,
          width: (labels[10][language].length*8)+60,
          height: 27,
          left: '3%',
          borderRadius: 5,
          backgroundColor: '#00593a',
          overflow: 'hidden'
        }}>
          <View>
            <View style={{
              width: 50,
              height: 20,
              backgroundColor: '#eaaf39'
            }}/>
            <View style={{
              width: 50,
              height: 7,
              backgroundColor: '#ff5722'
            }}/>
          </View>
          <Text style={{
            fontSize: 11,
            color: 'white',
            paddingLeft: 10
          }}>{labels[10][language]}</Text>
        </View>
        <Pressable
          onPress={() => navigation.navigate('Settings', {})}
          style={styles.settingsContainer}>
          <View style={styles.settingsButton}>
            <Image
              style={styles.settingsImage}
              source={SETTINGS}/>
          </View>
        </Pressable>
        {buttonsTemplate}
      </ScrollView>
      <View style={styles.navContainer}>
        <Pressable
          onPress={() => searchPressed()}>
          <View style={{...styles.navButton, width: navButtonWidth}}>
            <View style={styles.navButtonView}>
              <Image
                style={styles.navButtonImage}
                source={SEARCH}/>
              <Text style={styles.navButtonText}>{labels[0][language]}</Text>
            </View>
          </View>
        </Pressable>
        <Pressable
          onPress={() => libraryPressed()}>
          <View style={{...styles.navButton, width: navButtonWidth}}>
            <View style={styles.navButtonView}>
              <Image
                style={styles.navButtonImage}
                source={LIBRARY}/>
              <Text style={styles.navButtonText}>{labels[1][language]}</Text>
            </View>
          </View>
        </Pressable>
        <Pressable
          onPress={() => aboutPressed()}>
          <View style={{...styles.navButton, width: navButtonWidth}}>
            <View style={styles.navButtonView}>
              <Image
                style={styles.navButtonImage}
                source={INFO}/>
              <Text style={styles.navButtonText}>{labels[2][language]}</Text>
            </View>
          </View>
        </Pressable>
      </View>
    </View>

  );
}

const styles = StyleSheet.create({
  mainContainer: {
    width: "100%",
    backgroundColor: '#00593a',
    alignItems: 'center',
    justifyContent: 'center',
  },
  scrollContainer: {
    width: "100%",
    backgroundColor: '#00593a',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 0,
    margin: 0
  },
  subParent: {
    width: "100%",
    alignItems: 'center',
    justifyContent: 'center',
    padding: 0,
    margin: 0,
  },
  subContainer: {
    width: "100%",
    // borderRadius: 5,
    overflow: "hidden",
    maxHeight: 0,
    alignItems: 'center',
    justifyContent: 'center',
    // padding: 0,
    // margin: 0,
  },
  button: {
    flexDirection: 'row',
    height: 80,
    width: "100%",
    marginBottom: 5,
    borderRadius: 7,
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  buttonLS: {
    flexDirection: 'row',
    height: 80,
    width: "100%",
    marginBottom: 5,
    borderRadius: 7,
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  subButton: {
    borderRightWidth: 10
  },
  categoryTouchArea: {
    width: '100%',
    height: 80,
    justifyContent: 'center'
  },
  categoryButton: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  buttonText: {
    marginLeft: 15,
    fontSize: 17,
    fontWeight: "600",
    color: 'white'
  },
  buttonTextLS: {
    fontSize: 14,
    fontWeight: "600",
    color: '#4caf50'
  },
  expandImage: {
    height: 20,
    width: 20,
    margin: 15,
  },
  navContainer: {
    position: "absolute",
    width: "100%",
    height: 65,
    bottom: 0,
    left: 0,
    backgroundColor: "black",
    flexDirection: "row",
    justifyContent: 'center',
    alignItems: 'center',
  },
  navButton: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  navButtonImage: {
    height: 23,
    width: 23
  },
  navButtonView: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  navButtonText: {
    color: 'white',
    fontSize: 12,
    padding: 4
  },
  statsContainer: {
    marginBottom: 15,
    padding: 5
  },
  statsSubContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  statsText: {
    color: '#a2a2a2'
  },
  LSText: {
    color: 'white',
    fontSize: 16,
    margin: 20
  },
  settingsContainer: {
    position: 'absolute',
    right: 7,
    top: 7
  },
  settingsButton: {
    width: 40,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#4b8c72',
    borderRadius: 7,
    borderWidth: 1,
    borderColor: '#7ebfa7'
  },
  settingsImage: {
    height: 25,
    width: 25,
    padding: 7.5
  }
});

export default Categories;
