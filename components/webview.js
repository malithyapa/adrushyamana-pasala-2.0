import React, { useState, useCallback, useRef } from "react";
import { StyleSheet, Text, View, Dimensions, Image} from 'react-native';
import { WebView } from 'react-native-webview';
import { LOGO } from '../images/index';

const Webview = (props) => {

  const url = props.route.params.url.trim();
  const imageWidth = Dimensions.get('window').width;

  return (
    <WebView source={{ uri: url }} />
  );
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: '#00593a'
  },
  aboutText: {
    fontSize: 15,
    color: 'white',
    marginBottom: 20,
    padding: 10
  }
});

export default Webview;
