import React, { useState, useCallback, useRef } from "react";
import { StyleSheet, Text, View, Dimensions, Image, ScrollView, Linking, Pressable } from 'react-native';

import { LOGO, MAIL, FACEBOOK, SPOTIFY, PATREON, BLOG, BACK } from '../images/index';

import { useSelector, useDispatch } from 'react-redux'

const About = (props) => {

  const imageWidth = Dimensions.get('window').width;

  let language = useSelector(state => state.language);

  const labels = [
    [
       `බහු හිස් සංකේතවාදය මූලික වශයෙන් සම්බන්ධ වී ඇත්තේ විෂ්ණු දෙවියන් සමඟය. එය පොදු අරමුණක් සඳහා සමුහයක් එකට වැඩ කිරීමේ අදහස සංකේතවත් කරයි. අද්රුෂ්‍යමාන පාසල යනු සාමූහික ප්‍රයත්නයක් ලෙස අපේ අධ්‍යාපන ක්‍රමයේ ඇති හිදැස් පියවා ගැනීමට දරන උත්සාහයකි.
       \n\nඅර්ධ ශතකයක් පුරාවට ශ්‍රී ලාංකික අධ්‍යාපන ක්‍රමය නොසලකා හැරීම හේතුවෙන් අපේ දැනුම් ක්ෂේස්ත්රයේ රික්තයක් ඇති වී ඇත. වයස් බාධාවක් නොමැතිව ඕනෑම කෙනෙකුට තමන් කැමති ක්ෂේත්‍රයක දැනුම ලබා ගැනීමේ නිදහස ඇති ගවේෂණාත්මක අධ්‍යාපන ක්‍රමයක් පිළිබඳව අපි විශ්වාස කරමු.
       \n\nමෙහි අන්තර්ගතයන්ගෙන් පැහැදිලි වන පරිදි, බොහෝ දැනුම් ක්ෂේත්‍රයන්ට අපේ මව් භාෂාවලින් අන්තර්ගතයක් නොමැති අතර, මව් භාෂාවලින් අධ්‍යාපනික අන්තර්ගතයන් නිර්මාණය කිරීමට අනුබල දීම සහ අවසානයේදී අපේම අන්තර්ගතයක් නිර්මාණය කිරීම තුලින් මෙම හිඩැස පිරවීම අපගේ අරමුණයි.
       \n\nඅද්රුෂ්‍යමාන පාසල ලාභ නොලබන ප්‍රයත්නයක් වන අතර සංවර්ධන කටයුතු සහ අන්තර්ගත නිර්මාණය කිරීම සඳහා පිරිවැය ඔබේ දායකත්වයෙන් ආවරණය කිරීමට සිදුවේ. ඔබට මූල්‍යමය සහයෝගයක් ලබා දිය හැකි නම්, හොඳ අදහස් තිබේ නම් හෝ වෙනත් ආකාරයකින් උදව් කළ හැකි නම් අප හා එක් වන්න.`,
       `The symbolism of multiple heads is primarily associated with the deity Vishnu. It symbolizes the idea of multiple individuals working together towards a common motive. Adrushyamana Pasala is an attempt to bridge the gaps in our education system as a collective effort.
       \n\nHalf a century of neglect of the Sri Lankan education system has created a vacuum in our knowledge sphere and we believe in an exploratory education system where anyone, without an age barrier, has the freedom to pursue knowledge acquisition in any field that interests them.
       \n\nAs is evident by the available content in the app most fields of knowledge lack content in our native languages, and we aim to fill this gap by supporting and eventually creating our own content.
       \n\nAdrushyamana pasala is a non-profit initiative and development work and content curation/creation costs will have to be covered by your contributions. If you can offer financial support, have great ideas or can help in any other way, please feel free to reach out.`,
       `பல தலைகளின் சின்னம் முதன்மையாக விஷ்ணு தெய்வத்துடன் தொடர்புடையது. இது ஒரு பொதுவான நோக்கத்தை நோக்கி பல தனிநபர்கள் ஒன்றிணைந்து செயல்படுவதைக் குறிக்கிறது. அத்ருஷ்யமான பாசலா என்பது ஒரு கூட்டு முயற்சியாக நமது கல்வி முறையில் உள்ள இடைவெளிகளைக் குறைக்கும் முயற்சி.
       \n\nபல வருடங்களாக இலங்கை கல்வி முறையை புறக்கணித்ததன் மூலம் நமது அறிவுத் துறையில் வெற்றிடத்தை உருவாக்கியுள்ளோம், எவருக்கும், எந்த வயதிலும் தடையின்றி எந்த துறையிலும் அறிவு பெறுதலைத் தொடர சுதந்திரம் இருக்கும் ஒரு ஆய்வு கல்வி முறையை நாங்கள் நம்புகிறோம்.
       \n\nபயன்பாட்டில் உள்ள உள்ளடக்கத்தால் தெளிவாகத் தெரிந்தபடி, பெரும்பாலான அறிவுத் துறைகளுக்கு நம் தாய் மொழியில் உள்ளடக்கம் இல்லை, மேலும் இந்த இடைவெளியை ஆதரிப்பதன் மூலம் நிரப்பவும், இறுதியில் எங்கள் சொந்த உள்ளடக்கத்தை உருவாக்கவும் நாங்கள் விரும்புகிறோம்.
       \n\nஅத்ருஷ்யமனா பாசலா ஒரு இலாப நோக்கற்ற முயற்சி மற்றும் மேம்பாட்டு வேலை மற்றும் உள்ளடக்கத் தொகுப்பு/உருவாக்கும் செலவுகள் உங்கள் பங்களிப்புகளால் ஈடுசெய்யப்பட வேண்டும். நீங்கள் நிதி உதவி வழங்கலாம், சிறந்த யோசனைகள் இருந்தால் அல்லது வேறு எந்த வகையிலும் உதவ முடியும் என்றால், தயவுசெய்து அணுகவும்.`
    ],
  ]

  return (
    <ScrollView>
      <View style={styles.screenHeader}>
        <Pressable
          onPress={() => props.navigation.navigate('Categories', {})}>
          <View style={styles.backButtonTouchArea}>
            <Image
              style={{height: 25, width: 25}}
              source={BACK}/>
          </View>
        </Pressable>
        <View style={styles.screenName}>
          <Text style={styles.screenNameText}>අපි ගැන (About)</Text>
        </View>
      </View>
      <View style={styles.mainContainer}>
        <Image style={{ height: 400, width: imageWidth}}
              source={LOGO}/>
        <Text style={styles.aboutText}>
          {labels[0][language]}
        </Text>
        <View style={{marginBottom: 10}}>
          <View style={styles.contactView}>
            <Image
              style={styles.contactImage}
              source={PATREON}/>
            <Text
              style={styles.contactText}
              onPress={() => {
                Linking.openURL('https://www.patreon.com/malithyapa?fan_landing=true');
              }}>
              patreon.com/adrushyamana
            </Text>
          </View>
          <View style={styles.contactView}>
            <Image
              style={styles.contactImage}
              source={MAIL}/>
            <Text
              style={styles.contactText}
              onPress={() => {
                Linking.openURL('mailto: contact@adrushyamana.tech');
              }}>
              contact@adrushyamana.tech
            </Text>
          </View>
        </View>
        <View style={styles.teamView}>
          <Text
            style={styles.team}>
            Team :
          </Text>
          <View style={{marginBottom: 10}}>
              <Text
                style={styles.member}>
                Malith Yapa - Creator
              </Text>
              <View style={styles.contactView2}>
                <Image
                  style={styles.contactImage2}
                  source={FACEBOOK}/>
                <Text
                  style={styles.contactText2}
                  onPress={() => {
                    Linking.openURL('https://www.facebook.com/malithyapa');
                  }}>
                  facebook.com/malithyapa
                </Text>
              </View>
              <View style={styles.contactView2}>
                <Image
                  style={styles.contactImage2}
                  source={SPOTIFY}/>
                <Text
                  style={styles.contactText2}
                  onPress={() => {
                    Linking.openURL('https://open.spotify.com/user/31ifaitojpi5odtxys4tqcwkpyxi');
                  }}>
                  spotify.com/malithyapa
                </Text>
              </View>
            </View>
            <View style={{marginBottom: 10}}>
              <Text
                style={styles.member}>
                Yasitha Rajapakshe - Quality
              </Text>
              <View style={styles.contactView2}>
                <Image
                  style={styles.contactImage2}
                  source={FACEBOOK}/>
                <Text
                  style={styles.contactText2}
                  onPress={() => {
                    Linking.openURL('https://www.facebook.com/yasiraj026');
                  }}>
                  facebook.com/yasiraj
                </Text>
              </View>
            </View>
        </View>
        <View style={styles.teamView}>
          <Text
            style={styles.team}>
            Contributors :
          </Text>
          <Text
            style={styles.member}>
            Mahesh Chathuranga
          </Text>
          <View style={styles.contactView2}>
            <Image
              style={styles.contactImage2}
              source={FACEBOOK}/>
            <Text
              style={styles.contactText2}
              onPress={() => {
                Linking.openURL('https://www.facebook.com/mahesh.lcop');
              }}>
              facebook.com/mahesh
            </Text>
          </View>
          <Text
            style={styles.member}>
            Shehani Tissera
          </Text>
          <View style={styles.contactView2}>
            <Image
              style={styles.contactImage2}
              source={BLOG}/>
            <Text
              style={styles.contactText2}
              onPress={() => {
                Linking.openURL('https://thebiolog.com/');
              }}>
              thebiolog.com
            </Text>
          </View>
        </View>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: '#00593a',
    paddingBottom: 20
  },
  aboutText: {
    fontSize: 15,
    color: 'white',
    marginBottom: 5,
    padding: 15
  },
  contactImage: {
    height: 20,
    width: 20
  },
  contactImage2: {
    height: 15,
    width: 15
  },
  contactView: {
    paddingLeft: 15,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    width: '100%',
    color: 'white'
  },
  contactView2: {
    paddingLeft: 5,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    width: '100%',
    color: 'white'
  },
  contactText: {
    color: 'white',
    padding: 5,
    paddingLeft: 10
  },
  contactText2: {
    color: 'white',
    padding: 5,
    paddingLeft: 10,
    fontSize: 14
  },
  screenHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    padding: 0,
    margin: 0,
    height: 80,
    backgroundColor: '#194437'
  },
  backButtonTouchArea: {
    width: '10%',
    padding: 15,
  },
  screenName: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  screenNameText: {
    marginLeft: 15,
    fontSize: 17,
    fontWeight: "600",
    color: 'white'
  },
  teamView: {
    marginTop: 10,
    paddingLeft: 10,
    justifyContent: 'flex-start',
    alignItems:'flex-start',
    width: '100%',
    color: 'white'
  },
  team: {
    color: '#8bc34a',
    padding: 5,
  },
  member: {
    color: '#a8abbd',
    fontSize: 14,
    padding: 5,
  }
});

export default About;
