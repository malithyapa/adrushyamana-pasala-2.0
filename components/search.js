import React, { useState, useCallback, useRef } from "react";
import { StyleSheet, Text, View, ScrollView, TextInput, Pressable, Image, Dimensions} from 'react-native';
import { BACK } from '../images/index';
import { useDispatch, useSelector } from 'react-redux';

const Typesense = require('typesense')

let client = new Typesense.Client({
  'nodes': [{
    'host': 'apjq3xh0v4srb7kgp-1.a1.typesense.net',
    'port': '443',
    'protocol': 'https'
  }],
  'apiKey': 'DgcNxeDoPNfvn4sAnMBllUMf3RYeTjtu',
  'connectionTimeoutSeconds': 5
})

String.prototype.hashCode = function() {
  var hash = 0, i, chr;
  if (this.length === 0) return hash;
  for (i = 0; i < this.length; i++) {
    chr   = this.charCodeAt(i);
    hash  = ((hash << 5) - hash) + chr;
    hash |= 0;
  }
  return hash;
};

const getRGBFromString = (string) => {
  const hash = string.hashCode();
  const r = (hash & 0xFF0000) >> 16;
  const g = (hash & 0x00FF00) >> 8;
  const b = hash & 0x0000FF;
  return `rgba(${r}, ${g}, ${b}, 0.2)`
}

const Search = (props) => {

  const [searchQuery, setSearchQuery] = useState('');
  const [results, setResults] = useState([]);

  const labels = [
    ['ප්‍රතිඵල හමු නොවිණි.', 'No results found.', 'முடிவுகள் எதுவும் இல்லை.'],
    ['උදා: දර්ශණය', 'Ex: Philosophy', 'உதாரணம்: தத்துவம்'],
  ];

  let language = useSelector(state => state.language);

  const dimensions = Dimensions.get('window');
  const imageHeight = Math.round(dimensions.width / 2.048);
  const screenWidth = dimensions.width;
  const buttonTextWidth = screenWidth-135;

  let [progressText, setProgressText] = useState('...        ');
  let [progressIndex, setProgressIndex] = useState(0);
  let [inProgress, setInProgress] = useState(false);
  let [mounted, setMounted] = useState(false);

  React.useEffect(() => {
    mounted = true;
    setMounted(mounted);
    return () => {
      mounted = false;
      setMounted(mounted);
    }
  });

  React.useEffect(() => {
    const interval = setInterval(() => {
      if(mounted) {
        let string = ''
        for(let i=0;i<8;i++) {
          let j = progressIndex;
          if(progressIndex>7) {
            j = 15 - progressIndex;
          }
          if(i != j) {
            string += ' ';
          } else {
            string += '...';
          }
        }
        progressText = string;
        setProgressText(progressText);
        if(progressIndex < 15)
          progressIndex++;
        else
          progressIndex = 0;
        setProgressIndex(progressIndex);
      }
    }, 500);
    return () => clearInterval(interval);
  }, [])

  let loadingTemplate = (
    <View style={{
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center'
    }}>
      <Text style={{color: 'white', fontSize: 17}}>
        {progressText}
      </Text>
    </View>
  );

  const executeSearch = () => {
    inProgress = true;
    setInProgress(inProgress);
    let searchParameters = {
      'q'         : searchQuery,
      'query_by'  : 'title,description,categories',
      'per_page'  : 50
    }
    client.collections('Content')
      .documents()
      .search(searchParameters)
      .then(res => {
        if(res.hits.length) {
          setResults(res.hits.map((doc, index) => {
            let categoryTemplate = []
            doc.document.categories.map((cat, index2) => {
              categoryTemplate.push(
                <View
                  key={index2}
                  style={{backgroundColor: '#44675b'}}>
                  <View
                    key={cat}
                    style={{
                      flexDirection: 'row',
                      backgroundColor: getRGBFromString(cat),
                      padding: 5
                    }}>
                    <Text style={{color: 'white', fontSize: 13}}>{cat}</Text>
                  </View>
                </View>
              );
            });
            inProgress = false;
            setInProgress(inProgress);
            return (
            <View
              style={{
                marginBottom: 10,
                backgroundColor: 'black'
              }}
              key={index}>
              <Pressable
                key={index}
                onPress={() => {
                  props.navigation.navigate('Video', {documentId: doc.document.documentId, fromSearch: true})
                }}>
                <View style={styles.subButton} >
                  {!!doc.document.videoId && (
                    <Image
                    style={styles.thumbnail}
                    source={{uri: doc.document.thumbnail}}/>
                  )}
                  <Text style={{...styles.buttonText, width: buttonTextWidth}}>{doc.document.title.substring(0,80) + '...'}</Text>
                </View>
              </Pressable>
              {categoryTemplate}
            </View>
          )}))
        } else {
          setResults((
            <View
              key={'placeholder'}
              style={styles.placeholder}>
              <Text style={styles.placeholderText}>{labels[0][language]}</Text>
            </View>
          ));
          inProgress = false;
          setInProgress(inProgress);
        }
      });
  }

  return (
    <ScrollView style={styles.mainContainer}>
      <View style={styles.screenHeader}>
        <Pressable
          onPress={() => props.navigation.navigate('Categories', {})}>
          <View style={styles.backButtonTouchArea}>
            <Image
              style={{height: 25, width: 25}}
              source={BACK}/>
          </View>
        </Pressable>
        <View style={styles.screenName}>
          <Text style={styles.screenNameText}>සොයන්න (Search)</Text>
        </View>
      </View>
      <TextInput
        style={styles.searchText}
        color={'black'}
        placeholderTextColor={'grey'}
        placeholder={labels[1][language]}
        onSubmitEditing={executeSearch}
        onChangeText={setSearchQuery}>
      </TextInput>
      {inProgress && loadingTemplate}
      {results}
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  mainContainer: {
    flexGrow: 1,
    backgroundColor: '#00593a'
  },
  searchText: {
    backgroundColor: 'white',
    borderRadius: 5,
    margin: 10,
    marginBottom: 20,
    marginTop: 20,
    padding: 10,
    fontSize: 15
  },
  subButton: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: "100%",
    height: 100,
    backgroundColor: 'rgba(16, 16, 16, 0.8)',
    borderBottomWidth: 1,
    borderBottomColor: 'black'
  },
  thumbnail: {
    height: 100,
    width: 133,
  },
  placeholder: {
    padding: 10
  },
  placeholderText: {
    color: 'white',
    fontSize: 16
  },
  screenHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    padding: 0,
    margin: 0,
    height: 80,
    backgroundColor: '#194437'
  },
  backButtonTouchArea: {
    width: '10%',
    padding: 15,
  },
  screenName: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  screenNameText: {
    marginLeft: 15,
    fontSize: 17,
    fontWeight: "600",
    color: 'white'
  },
  buttonText: {
    padding: 5,
    color: 'white'
  }
});

export default Search;
