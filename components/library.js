import React, { useState, useCallback, useRef } from "react";
import { StyleSheet, Text, View, ScrollView, Pressable, Image, Linking, Dimensions} from 'react-native';
import firestore from '@react-native-firebase/firestore';
import { getUniqueId } from 'react-native-device-info';
import { LIBRARY_G, RIGHT, BACK, UP, DOWN, BIN } from '../images/index';
import { handleError } from '../helpers/errorHandling';

import { useSelector, useDispatch } from 'react-redux'

String.prototype.hashCode = function() {
  var hash = 0, i, chr;
  if (this.length === 0) return hash;
  for (i = 0; i < this.length; i++) {
    chr   = this.charCodeAt(i);
    hash  = ((hash << 5) - hash) + chr;
    hash |= 0;
  }
  return hash;
};

const getRGBFromString = (string) => {
  const hash = string.hashCode();
  const r = (hash & 0xFF0000) >> 16;
  const g = (hash & 0x00FF00) >> 8;
  const b = hash & 0x0000FF;
  return `rgb(${r}, ${g}, ${b})`
}

const CATEGORY_MAX = 200;
const Library = (props) => {

  const dispatch = useDispatch();
  const email = useSelector((state) => state.email);

  const [expandAnim, setExpandAnim] = useState(Array(CATEGORY_MAX).fill(0));
  const [expandedFlag, setExpandedFlag] = useState(Array(CATEGORY_MAX).fill(false));

  let library = useSelector((state) => state.library);
  let libraryCategories = useSelector((state) => Object.keys(state.library));

  const dimensions = Dimensions.get('window');
  const imageHeight = Math.round(dimensions.width / 2.048);
  const screenWidth = dimensions.width;
  const buttonTextWidth = screenWidth-165;
  const videoButtonWidth = screenWidth-30;

  const labels = [
    ['පුස්තකාලයට එකතු කළ අයිතම, හෝ සටහන් මෙහි දිස්වනු ඇත.', 'Items added to library, and Notes will appear here.', 'நூலகத்தில் சேர்க்கப்பட்ட பொருட்கள் அல்லது குறிப்புகள் இங்கே தோன்றும்.'],
  ];

  let language = useSelector(state => state.language);

  const buttonPressed = (index, cat) => {

    let subHeight = 0;
    if(cat === 'සටහන් (Notes)') {
      subHeight = Object.keys(library['සටහන් (Notes)']).length*150;
    } else {
      subHeight = library[cat].length*150;
    }

    if(!expandedFlag[index]) {
      expandAnim[index] = subHeight;
      setExpandAnim(Array.from(expandAnim));
      expandedFlag[index] = true;
    } else {
      expandAnim[index] = 0;
      setExpandAnim(Array.from(expandAnim));
      expandedFlag[index] = false;
    }
  }

  const removeFromLibrary = (cat, index) => {
    library[cat].splice(index, 1);
    dispatch({type: 'setLibrary', values: library});

    firestore().collection(DEV_DB? 'Users-dev': 'Users')
      .doc(email)
      .update({library})
      .catch(err => handleError(err, 'content:removeFromLibrary'));
  }
  const buttonsTemplate = [];

  if(library['සටහන් (Notes)'] && Object.keys(library['සටහන් (Notes)']).length) {
    buttonsTemplate.push(
      <Pressable
        key={0}
        onPress={() => buttonPressed(0, 'සටහන් (Notes)')}>
        <View style={{...styles.button, backgroundColor: '#1f9a4c'}}>
          <Text style={styles.buttonText}>සටහන් (Notes)</Text>
          <View style={styles.categoryExpand}>
            <Text>({Object.keys(library['සටහන් (Notes)']).length})</Text>
            <Image
              style={styles.expandImage}
              source={expandedFlag[0] ? UP : DOWN}/>
          </View>
        </View>
      </Pressable>
    )
    const subButtonsTemplate = [];
    Object.keys(library['සටහන් (Notes)']).map((docId, index2) => {
      const video = library['සටහන් (Notes)'][docId].video;
      subButtonsTemplate.push((
        <View
          style={{...styles.subButton, width: screenWidth}}
          key={index2}>
          <Pressable
            onPress={() => {
              props.navigation.navigate('Video', {documentId: video.documentId, fromLibrary: true})
            }}>
            <View style={{flexDirection: 'row', alignItems: 'center', width: '100%'}}>
              <Image
                style={styles.thumbnail}
                source={{uri: video.thumbnail}}/>
              <Text style={{...styles.thumbnailText, width: buttonTextWidth}}>{video.title.substring(0,80) + '...'}</Text>
            </View>
          </Pressable>
        </View>
      ))
    })
    buttonsTemplate.push(
      <View key={'sub0'}style={styles.subParent}>
        <View style={{...styles.subContainer, maxHeight: expandAnim[0]}}>
          {subButtonsTemplate}
          <View style={{marginBottom: 10}}></View>
        </View>
      </View>
    )
  }

  libraryCategories
    .filter(category => category != 'සටහන් (Notes)')
    .map((cat, index1) => {
      index1 = index1 + 1;
      if(!library[cat].length) return;
      buttonsTemplate.push(
        <Pressable
          key={index1}
          onPress={() => buttonPressed(index1, cat)}>
          <View style={{...styles.button, backgroundColor: '#006e6a'}}>
            <Text style={styles.buttonText}>{cat}</Text>
            <View style={styles.categoryExpand}>
              <Text>({library[cat].length})</Text>
              <Image
                style={styles.expandImage}
                source={expandedFlag[index1] ? UP : DOWN}/>
            </View>
          </View>
        </Pressable>
      )
      const subButtonsTemplate = [];
      library[cat].map((video, index2) => {
        subButtonsTemplate.push((
          <View
            style={{...styles.subButton}}
            key={index2}>
            <Pressable
              onPress={() => {
                if(video.videoId) {
                  if(video.embeddable == 1) {
                    props.navigation.navigate('Video', {documentId: video.documentId, fromLibrary: true})
                  } else {
                    Linking.openURL('https://www.youtube.com/watch?v=' + video.documentId);
                  }
                }
                if(video.link) {
                  props.navigation.navigate('WebView', {url: video.link})
                }
              }}>
              <View style={{flexDirection: 'row', alignItems: 'center', width: videoButtonWidth}}>
                {!!video.videoId && (
                  <Image
                  style={styles.thumbnail}
                  source={{uri: video.thumbnail}}/>
                )}
                <Text style={{...styles.thumbnailText, width: buttonTextWidth}}>{video.title.substring(0,80) + '...'}</Text>
              </View>
            </Pressable>
            <Pressable
              onPress={() => removeFromLibrary(cat, index2)}>
              <View style={{...styles.deleteButton, backgroundColor: '#006e6a', width: 30}}>
                <Image
                  style={styles.bin}
                  source={BIN}/>
              </View>
            </Pressable>
          </View>
        ))
      })
      buttonsTemplate.push(
        <View key={'sub' +index1}style={styles.subParent}>
          <View style={{...styles.subContainer, maxHeight: expandAnim[index1]}}>
            {subButtonsTemplate}
            <View style={{marginBottom: 10}}></View>
          </View>
        </View>
      )
  });

  if(!buttonsTemplate.length) {
    buttonsTemplate.push(
      <View
        key={'placeholder'}
        style={styles.placeholder}>
        <Text style={styles.placeholderText}>{labels[0][language]}</Text>
      </View>
    )
  }
  return (
    <ScrollView contentContainerStyle={styles.mainContainer}>
      <View style={styles.screenHeader}>
        <Pressable
          onPress={() => props.navigation.navigate('Categories', {})}>
          <View style={styles.backButtonTouchArea}>
            <Image
              style={{height: 25, width: 25}}
              source={BACK}/>
          </View>
        </Pressable>
        <View style={styles.screenName}>
          <Text style={styles.screenNameText}>පුස්තකාලය (Library)</Text>
        </View>
      </View>
      <View style={styles.imageContainer}>
        <Image
          style={styles.libraryImage}
          source={LIBRARY_G}/>
      </View>
      {buttonsTemplate}
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  mainContainer: {
    flexGrow: 1,
    backgroundColor: '#00593a',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    width: '100%',
    minHeight: '100%'
  },
  imageContainer: {
    flexDirection: "row",
    justifyContent: 'center',
    width: '100%'
  },
  libraryImage: {
    height: 80,
    width: 80,
    margin: 30
  },
  button: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: "100%",
    height: 80,
    paddingLeft: 15,
    paddingRight: 15,
    marginBottom: 5,
  },
  buttonText: {
    fontSize: 17,
    fontWeight: "600",
    color: 'white',
    width: '80%',
    padding: 5,
    paddingLeft: 0
  },
  thumbnailText: {
    padding: 7,
    color: 'white'
  },
  subButton: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: "100%",
    backgroundColor: '#001c1c',
    borderBottomWidth: 1,
    borderBottomColor: 'black'
  },
  subContainer: {
    width: "100%",
    overflow: 'hidden',
    maxHeight: 0,
    padding: 0,
    margin: 0,
  },
  subParent: {
    marginBottom: 5
  },
  placeholder: {
    padding: 10
  },
  placeholderText: {
    color: 'white',
    fontSize: 16
  },
  screenHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    padding: 0,
    margin: 0,
    height: 80,
    backgroundColor: '#194437'
  },
  backButtonTouchArea: {
    width: '10%',
    padding: 15,
  },
  screenName: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  screenNameText: {
    marginLeft: 15,
    fontSize: 17,
    fontWeight: "600",
    color: 'white'
  },
  expandImage: {
    height: 20,
    width: 20,
    margin: 15
  },
  categoryExpand: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: '10%'
  },
  thumbnail: {
    height: 100,
    width: 133
  },
  bin: {
    height: 20,
    width: 20
  },
  videoTouchArea: {
    width: '90%'
  },
  deleteButton: {
    width: 30,
    padding: 7.5,
    height: 100,
    alignItems: 'center',
    justifyContent: 'center',

  }
});

export default Library;
