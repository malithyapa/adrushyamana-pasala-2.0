import React, { useState, useCallback, useRef } from "react";
import { StyleSheet, Text, View, ScrollView, Pressable, Image, Dimensions, Animated, Linking, BackHandler} from 'react-native';

import AnimatedLoader from "react-native-animated-loader";
import YoutubePlayer from "react-native-youtube-iframe";

import { LIBRARY_B, PLAY, FAV_B, FAV_Y, UP_W, DOWN_W, BACK } from '../images/index';

import firestore from '@react-native-firebase/firestore';
import { getUniqueId } from 'react-native-device-info';
import Toast from 'react-native-simple-toast';

import { useSelector, useDispatch } from 'react-redux';
import NetInfo from "@react-native-community/netinfo";
import CheckBox from '@react-native-community/checkbox';
import { useFocusEffect } from '@react-navigation/native';

import { handleError } from '../helpers/errorHandling';

const Content = (props) => {

  const dispatch = useDispatch();

  useFocusEffect(() => {
    const backAction = () => {
      props.navigation.navigate('Categories', {});
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      backAction
    );

    return () => backHandler.remove();
  });

  const labels = [
    ['පුස්තකාලයට දමන්න ', 'Add to Library', 'நூலகத்தில் சேர்க்கவும்'],
    ['පුස්තකාලයෙන් ඉවත් කරන්න', 'Remove from Library', 'நூலகத்திலிருந்து அகற்று'],
    ['වීඩියෝවට යන්න', 'Go to Video', 'காணொளிக்கு செல்'],
    ['ලිපියට යන්න', 'Go to Article', 'கட்டுரைக்குச் செல்லவும்்'],
    ['පුස්තකාලයට දමන ලදීි', 'Added to library', 'நூலகத்தில் சேர்க்கப்பட்டது'],
    ['පුස්තකාලයෙන් ඉවත් කරන ලදී', 'Removed from library', 'நூலகத்திலிருந்து அகற்றப்பட்டது'],
    ['රැඳී සිටින්න..', 'Please wait..', 'காத்திருக்கவும்..'],
    ['ඔබ අන්තර්ජාලයට සම්බන්ද වී නැත', 'You\'re not connected to the internet','நீங்கள் இணையத்துடன் இணைக்கப்படவில்லை'],
    ['විෂය ප්‍රියතම විෂයන්ට එකතු කරන ලදී', 'Category added to favourites', 'பிடித்தவற்றில் வகை சேர்க்கப்பட்டது'],
    ['භාෂාවන්', 'Languages', 'மொழிகள்'],
    ['මෙම භාෂාවලින් අන්තර්ගතයන් නොමැත', 'No content in these languages', 'இந்த மொழிகளில் உள்ளடக்கம் இல்லை'],
    ['සොයන්න', 'Filter', 'வடிகட்டி']
  ];

  let [videos, setVideos] = useState([]);
  let leaving = false;
  let [fetchedIndices, setFetchedIndices] = useState([]);
  let [spentIndices, setSpentIndices] = useState(new Set());

  const category = props.route.params.sub.trim();
  let categories = useSelector(state => state.categories);

  React.useEffect(async () => {
    const traverseArray = (array) => {
      for(const cat of array) {
        if(cat.name == category) {
          cat.lastVisited = new Date().getTime();
          break;
        }
        if(cat.subCategories) {
          traverseArray(cat.subCategories);
        }
      }
    }

    traverseArray(categories);

    await firestore().collection(DEV_DB? 'Users-dev': 'Users').doc(email).update({
      categories: categories
    }).catch((err) => handleError(err, 'content:updateLastVisited'));
  }, [])

  const [videoVisible, setVideoVisible] = useState(Array(10).fill(false));
  const [videoReadyCount, setVideoReadyCount] = useState(0);

  const windowHeight = Dimensions.get('window').height;

  let [numElements, setNumElements] = useState(1);
  let [numElementsWindow, setNumElementsWindow] = useState(5);

  let [allContentLoaded, setAllContentLoaded] = useState(false);

  let [requestsSent, setRequestsSent] = useState(0);
  let [requestsFailed, setRequestsFailed] = useState(0);

  let email = useSelector(state => state.email);

  let [mounted, setMounted] = useState(true);

  let language = useSelector(state => state.language);
  const DEV_DB = useSelector(state => state.dev);
  let contentLanguages = useSelector(state => state.contentLanguages);
  const contentMax = useSelector(state => state.contentMax);

  const dimensions = Dimensions.get('window');
  const screenWidth = dimensions.width;
  const thumbnailHeight = screenWidth*0.625;

  let favCategories = useSelector(state => state.favCategories) || [];
  let [favourite, setFavourite] = useState(favCategories.includes(category));

  const [showFilter, setShowFilter] = useState(false);
  let [reloadIndex, setReloadIndex] = useState(0);

  const [promises, setPromises] = useState([]);
  let [languageFilterText, setLanguageFilterText] = useState(labels[11][language]);
  let stats = useSelector(state => state.stats);

  let sinhalaCount = stats[category].Videos.Sinhala + stats[category].Articles.Sinhala;
  let englishCount = stats[category].Videos.English + stats[category].Articles.English;
  let tamilCount = stats[category].Videos.Tamil + stats[category].Articles.Tamil;

  let [sinhalaFilter, setSinhalaFilter] = useState(props.route.params.languages ? props.route.params.languages[0] : (sinhalaCount != 0));
  let [englishFilter, setEnglishFilter] = useState(props.route.params.languages ? props.route.params.languages[1] : (englishCount != 0));
  let [tamilFilter, setTamilFilter] = useState(props.route.params.languages ? props.route.params.languages[2] : (tamilCount != 0));

  let [disableFavorite, setDisableFavorite] = useState(false);
  const MAX_CONTENT = stats[category].Videos.Sinhala
    + stats[category].Videos.English
    + stats[category].Videos.Tamil
    + stats[category].Articles.Sinhala
    + stats[category].Articles.English
    + stats[category].Articles.Tamil;

  let [disableAddToLibrary, setDisableAddToLibrary] = useState(Array(MAX_CONTENT).fill(false));

  let queryTemp = [];
  if(contentLanguages[0]) queryTemp.push('Sinhala');
  if(contentLanguages[1]) queryTemp.push('English');
  if(contentLanguages[2]) queryTemp.push('Tamil');
  if(!queryTemp.length) queryTemp.push('NA');
  let [query, setQuery] = useState(queryTemp);

  React.useEffect(() => {
    mounted = true;
    setMounted(true);
    return () => {
      mounted = false;
      setMounted(false);
    }
  }, []);

  React.useEffect(() => {
    const unsubscribe = NetInfo.addEventListener(state => {
      if(!state.isInternetReachable) {
        // mounted = false;
        // setMounted(mounted);
        props.navigation.navigate('Register', {networkError: true});
      }
    });
    return () => unsubscribe();
  }, []);

  const setFilter = (language, value) => {
    switch (language) {
      case 0:
        sinhalaFilter = value;
        setSinhalaFilter(sinhalaFilter);
        break;
      case 1:
        englishFilter = value;
        setEnglishFilter(englishFilter);
        break;
      case 2:
        tamilFilter = value;
        setTamilFilter(tamilFilter);
        break;
    }
  }
  if(props.route.params.reload) {
    setNumElements(1);
    setNumElementsWindow(5);
    setAllContentLoaded(false);
    setRequestsSent(0);
    setRequestsFailed(0)
    setVideos([]);
    setFetchedIndices([]);
    setSpentIndices(new Set());
    props.route.params.reload = false;
  }

  useFocusEffect(
    React.useCallback(() => {
      if(!allContentLoaded && ((requestsSent-requestsFailed) < numElements)) {

        // generate a random index that hasn't been tried before
        let randomIndex = null;
        while(!randomIndex || spentIndices.has(randomIndex)) {
          randomIndex = Math.floor(Math.random()*contentMax);
        }

        // update sent request count
        requestsSent++
        setRequestsSent(requestsSent);
        // send request

        promises.push(firestore().collection(DEV_DB? 'Content-dev': 'Content')
          .where('categories', 'array-contains', category)
          .where('language', 'in', query)
          .orderBy('queryIndex')
          .startAt(randomIndex)
          .limit(1)
          .get()
          .then(res => {
            //check if a document was returned
            if(!mounted) return;

            if(res.docs[0] && !query.includes(res.docs[0].data().language)) {
              return;
            }
            if(res.docs[0]) {
              // check if the returned document hasn't been fetched before
              if(!fetchedIndices.includes(res.docs[0].data().queryIndex)) {
                // add everything from randomIndex to queryIndex to spentIndices
                // as they should not be tried again
                for(let i=randomIndex; i<= res.docs[0].data().queryIndex; i++) {
                  spentIndices.add(i);
                }
                // stop loading content if all indices are exhausted
                if(spentIndices.size > (contentMax-1)) {
                  setAllContentLoaded(true);
                }
                setSpentIndices(new Set(spentIndices));

                // add queryIndex to fetched
                fetchedIndices.push(res.docs[0].data().queryIndex)
                setFetchedIndices(Array.from(fetchedIndices));

                // add retrieved document to videos, which will render it
                videos.push(res.docs[0].data());
                setVideos(Array.from(videos));

                // increment numElements so that a new request will be sent, unless the window is full
                if(numElements <= numElementsWindow) {
                  numElements++;
                  setNumElements(numElements);
                }
              } else {
                // update failed request count
                requestsFailed++
                setRequestsFailed(requestsFailed);

                // add everything from randomIndex to queryIndex to spentIndices
                // as they should not be tried again
                for(let i=randomIndex; i<= res.docs[0].data().queryIndex; i++) {
                  spentIndices.add(i);
                }
                setSpentIndices(new Set(spentIndices));
                // stop loading content if all indices are exhausted
                if(spentIndices.size > (contentMax-1)) {
                  setAllContentLoaded(true);
                }

              }
            } else {
              // update failed request count
              requestsFailed++
              setRequestsFailed(requestsFailed);

              // add everything from randomIndex to contentMax to spentIndices
              // as they should not be tried again
              for(let i=randomIndex; i<= contentMax; i++) {
                spentIndices.add(i);
              }
              setSpentIndices(new Set(spentIndices));
              // stop loading content if all indices are exhausted
              if(spentIndices.size > (contentMax-1)) {
                setAllContentLoaded(true);
              }
            }
          })
          .catch(err => handleError(err, 'content:loadContent')));
          setPromises(promises);
      }
    }, [numElements, numElementsWindow, requestsFailed])
  );

  const scrollHandler = (event) => {
    const elemHeight = 480;
    if(event.nativeEvent.contentOffset.y > (elemHeight*numElementsWindow-windowHeight*2)) {
      numElementsWindow = numElementsWindow + 5;
      setNumElementsWindow(numElementsWindow);
      numElements++;
      setNumElements(numElements);
    }
  }

  let library = useSelector((state) => state.library);
  const documentsInLibrary = useSelector(state => state.documentsInLibrary);

  const addToLibrary = (video, index) => {
    disableAddToLibrary[index] = true;
    setDisableAddToLibrary(Array.from(disableAddToLibrary));

    Toast.show(labels[4][language]);
    video.categories.forEach((cat) => {
      if(!library[cat]) {
        library[cat] = [];
      }
      library[cat].push(video);
    });

    dispatch({type: 'setLibrary', values: library});

    firestore().collection(DEV_DB? 'Users-dev': 'Users')
      .doc(email)
      .update({library})
      .then(() => {
        disableAddToLibrary[index] = false;
        setDisableAddToLibrary(Array.from(disableAddToLibrary));
      })
      .catch(err => handleError(err, 'content:addToLibrary'));
  }

  const removeFromLibrary = (video, index) => {
    disableAddToLibrary[index] = true;
    setDisableAddToLibrary(Array.from(disableAddToLibrary));

    Toast.show(labels[5][language]);
    video.categories.forEach((cat) => {
      library[cat] = library[cat].filter(vid => {
        return vid.documentId !== video.documentId;
      })
    });
    dispatch({type: 'setLibrary', values: library});

    firestore().collection(DEV_DB? 'Users-dev': 'Users')
      .doc(email)
      .update({library})
      .then(() => {
        disableAddToLibrary[index] = false;
        setDisableAddToLibrary(Array.from(disableAddToLibrary));
      })
      .catch(err => handleError(err, 'content:removeFromLibrary'));
  }

  const playerReady = (index) => {
    videoVisible[index] = true;
    setVideoVisible(Array.from(videoVisible));
    setVideoReadyCount(videoReadyCount+1);
  }

  const addToFavourites = () => {
    disableFavorite = true;
    setDisableFavorite(disableFavorite);

    if(favCategories.includes(category)) {
      favCategories = favCategories.filter(cat => cat.localeCompare(category));
      favourite = false;
      setFavourite(favourite);
    } else {
      Toast.show(labels[8][language]);
      favCategories.push(category);
      favourite = true;
      setFavourite(favourite);
    }
    dispatch({type: 'setFavCategories', values: favCategories});

    firestore().collection(DEV_DB? 'Users-dev': 'Users')
      .doc(email)
      .update({favCategories})
      .then(() => {
        disableFavorite = false;
        setDisableFavorite(disableFavorite);
      })
      .catch(err => handleError(err, 'content:addToFavourites'));

  }

  let contentTemplate = [];

  videos.map((video, index) => {
    const shortDescription = video.description.substring(0, 80).replace('\n',' ') + '...';
    const title = video.title.substring(0,60) + '...';
    let languageTag = 'සි';
    if(video.language.localeCompare('English') === 0) {
      languageTag = 'En';
    }
    if(video.language.localeCompare('Tamil') === 0) {
      languageTag = 'த';
    }
    if(video.videoId) {
      let courseTag = <View></View>
      if(video.groupStatus == 2) {
        if(language == 0) {
          courseTag = (
            <View style={styles.courseTag}>
              <Text style={styles.courseTagText}>* වීඩියෝ {video.groupCount} සහිත පාඨමාලාවකි.</Text>
            </View>
          );
        }
        if(language == 1) {
          courseTag = (
            <View style={styles.courseTag}>
              <Text style={styles.courseTagText}>* This is a course with {video.groupCount} videos.</Text>
            </View>
          );
        }
        if(language == 2) {
          courseTag = (
            <View style={styles.courseTag}>
              <Text style={styles.courseTagText}>* இது {video.groupCount} வீடியோக்கள் கொண்ட பாடநெறி.</Text>
            </View>
          );
        }
      }
      contentTemplate.push(
        <View
          key={reloadIndex + '-' + index}
          style={{...styles.elementContainer, maxHeight: 590}}>
          {(video.embeddable == 1) && (
            <View>
              <YoutubePlayer
                style={styles.videoElement}
                height={240}
                videoId={video.videoId}
                useLocalHTML={true}
                key={reloadIndex + '*' + index}
                onReady={() => {
                  playerReady(index)
                }}
                play={video.active}/>
              <Pressable
                onLongPress={() => {}}
                onPress={() => {
                  video.active = true;
                }}
                style={{
                  position: 'absolute',
                  left: 0,
                  top: 0,
                }}>
                <View style={{
                  height: video.active ? 0 : 240,
                  width: screenWidth
                }}>
                </View>
              </Pressable>
            </View>
          )}
          {(video.embeddable == 0) && (
            <Pressable
              onPress={() => {
                Linking.openURL('https://www.youtube.com/watch?v=' + video.documentId);
              }}>
              <Image
                  style={{width: screenWidth, height: thumbnailHeight}}
                  source={{uri: video.thumbnail}}
                  onLayout={() => {
                    playerReady(index)
                  }}/>
            </Pressable>
          )}
          <Text style={styles.videoTitle}>
            {title}
          </Text>
          <View style={styles.detailsBar}>
            <Text style={styles.channelName}>{video.channel}</Text>
            <Text style={styles.language}>{languageTag}</Text>
          </View>
          {courseTag}
          <Text style={styles.shortDescription}>
            {shortDescription}
          </Text>
          <Pressable onPress={() => {
              if(documentsInLibrary.has(video.documentId)) {
                removeFromLibrary(video, index);
              } else {
                addToLibrary(video, index);
              }
            }}
            disabled={disableAddToLibrary[index]}>
            <View style={{...styles.libraryButton, backgroundColor: disableAddToLibrary[index] ? '#5a9e83' : '#549078'}}>
              <Image
                style={{ height: 25, width: 25}}
                source={LIBRARY_B}/>
              <Text style={styles.libraryButtonText}>
                {documentsInLibrary.has(video.documentId) ? labels[1][language] : labels[0][language]}
              </Text>
            </View>
          </Pressable>
          <Pressable onPress={() => {
              if(video.embeddable) {
                // mounted = false;
                // setMounted(mounted);
                props.navigation.navigate('Video', {documentId: video.documentId})
              } else {
                // mounted = false;
                // setMounted(mounted);
                Linking.openURL('https://www.youtube.com/watch?v=' + video.documentId);
              }
            }}>
            <View style={styles.libraryButton}>
              <Image
                style={{ height: 25, width: 25}}
                source={PLAY}/>
              <Text style={styles.libraryButtonText}>{labels[2][language]}</Text>
            </View>
          </Pressable>
        </View>
      )
    }

    if(video.link) {
      contentTemplate.push(
        <View
          key={index}
          style={{...styles.elementContainer, maxHeight: 590}}
          onLayout={() => {
            playerReady(index)
          }}>
          <Text style={styles.videoTitle}>
            {title}
          </Text>
          <View style={styles.detailsBar}>
            <Text style={styles.channelName}>{video.channel}</Text>
            <Text style={styles.language}>{languageTag}</Text>
          </View>
          <Text style={styles.shortDescription}>
            {shortDescription}
          </Text>
          <Pressable onPress={() => {
              if(documentsInLibrary.has(video.documentId)) {
                removeFromLibrary(video);
              } else {
                video
                addToLibrary(video);
              }
            }}>
            <View style={{...styles.libraryButton, backgroundColor: disableAddToLibrary[index] ? '#5a9e83' : '#549078'}}>
              <Image
                style={{ height: 25, width: 25}}
                source={LIBRARY_B}/>
              <Text style={styles.libraryButtonText}>
                {documentsInLibrary.has(video.documentId) ? labels[1][language] : labels[0][language]}
              </Text>
            </View>
          </Pressable>
          <Pressable onPress={() => {
              props.navigation.navigate('WebView', {url: video.link})
            }}>
            <View style={styles.libraryButton}>
              <Image
                style={{ height: 25, width: 25}}
                source={PLAY}/>
              <Text style={styles.libraryButtonText}>{labels[3][language]}</Text>
            </View>
          </Pressable>
        </View>
      )
    }
  });

  let initialLoadingScreen = leaving || ((videoReadyCount == 0) && !(videos.length && (videos.length  == videoReadyCount)) && !allContentLoaded);

  React.useEffect(() => {
    const interval = setInterval(() => {
      if(mounted) {
        let string = ''
        for(let i=0;i<8;i++) {
          let j = progressIndex;
          if(progressIndex>7) {
            j = 15 - progressIndex;
          }
          if(i != j) {
            string += ' ';
          } else {
            string += '...';
          }
        }
        progressText = string;
        setProgressText(progressText);
        if(progressIndex < 15)
          progressIndex++;
        else
          progressIndex = 0;
        setProgressIndex(progressIndex);
      }
    }, 500);
    return () => clearInterval(interval);
  }, [])

  let [progressText, setProgressText] = useState('...        ');
  let [progressIndex, setProgressIndex] = useState(0);

  let loadingAnimation = <View></View>
  if(!allContentLoaded && !initialLoadingScreen) {
    loadingAnimation = (
      <View style={{
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        margin: 10,
        marginBottom: 20
      }}>
        <Text style={{color: 'white', fontSize: 17}}>
          {progressText}
        </Text>
      </View>
    );
  }

  return (
    <ScrollView
      style={{backgroundColor: "#00593a"}}
      onScroll={scrollHandler}>
      <View style={styles.screenHeader}>
        <Pressable
          onPress={() => {
            props.navigation.navigate('Categories', {})
          }}>
          <View style={styles.backButtonTouchArea}>
            <Image
              style={{height: 25, width: 25, marginLeft: 5}}
              source={BACK}/>
          </View>
        </Pressable>
        <View style={styles.categoryName}>
          <Text style={styles.buttonText}>{category}</Text>
          <Pressable
            onPress={() => addToFavourites()}
            disabled={disableFavorite}>
            <View style={styles.titleTouchArea}>
              <Image
                style={styles.starImage}
                source={favourite ? FAV_Y : FAV_B}/>
            </View>
          </Pressable>
        </View>
      </View>
      {contentTemplate}
      {allContentLoaded && (videos.length == 0) && (
        <Text style={styles.noContent}>{labels[10][language]}</Text>
      )}
      {loadingAnimation}
      <AnimatedLoader
        visible={initialLoadingScreen}
        overlayColor="#00593a"
        source={require("./9379-loader.json")}
        animationStyle={styles.lottie}
        speed={1}>
      </AnimatedLoader>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  elementContainer: {
    borderBottomColor: 'black',//'#489E3D',
    borderBottomWidth: 10
  },
  videoElement: {
    marginBottom: 10,
    paddingBottom: 0,
  },
  libraryButton: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#549078',
    height: 70,
    padding: 15,
    marginBottom: 7,
    borderRadius: 7
  },
  libraryButtonText: {
    padding: 7,
    fontSize: 15,
  },
  videoTitle: {
    maxHeight: 60,
    fontSize: 19,
    color: 'white',
    paddingTop: 4,
    paddingLeft: 5,
  },
  shortDescription: {
    maxHeight: 70,
    paddingLeft: 5,
    paddingBottom: 20,
    fontSize: 14,
    color: 'white'
  },
  lottie: {
    width: 80,
    height: 80
  },
  loadingView: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 200,
    padding: 50,
    width: '100%'
  },
  loadingText: {
    fontSize: 15,
    color: 'white'
  },
  detailsBar: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 5,
    paddingRight: 0
  },
  channelName: {
    fontSize: 13,
    color: '#22b07f'
  },
  language: {
    fontSize: 12,
    color: 'white',
    borderColor: '#22b07f',
    borderWidth: 1,
    padding: 3,
    paddingLeft: 5
  },
  courseTag: {
    padding: 3
  },
  courseTagText: {
    color: '#c1c1c1'
  },
  screenHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    padding: 0,
    margin: 0,
    height: 80,
    backgroundColor: '#194437'
  },
  backButtonTouchArea: {
    width: '10%',
    padding: 15
  },
  titleTouchArea: {
    width: '90%',
    justifyContent: 'center',
    padding: 0,
    margin: 0
  },
  categoryName: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingRight: 10
  },
  buttonText: {
    marginLeft: 10,
    fontSize: 17,
    fontWeight: "600",
    color: 'white',
    width: '70%'
  },
  starImage: {
    height: 20,
    width: 20,
    margin: 15
  },
  titleBar: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding:5
  },
  languageFilter: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: 'black',
    padding: 12
  },
  languageFilterTitle: {
    fontSize: 15,
    color: 'white'
  },
  arrowImage: {
    height: 20,
    width: 20,
    margin: 5
  },
  backImage: {

  },
  checkboxes: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#424141',
    overflow: 'hidden',
    margin: 0
  },
  checkboxesText: {
    color: 'white',
    fontSize: 14
  },
  checkboxLanguage: {
    margin: 10,
    flexDirection: 'row',
    alignItems: 'center',
    width: 100
  },
  noContent: {
    color: 'white',
    fontSize: 15,
    margin: 15
  },
  filterButton: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#545353',
    marginBottom: 15,
  },
  filterButtonText: {
    color: 'white',
    fontSize: 15,
    margin: 10
  }
});

export default Content;
