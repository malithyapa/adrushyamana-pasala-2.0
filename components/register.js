import React, { useState, useCallback, useRef } from "react";
import { StyleSheet, Text, View, TextInput, Image, Dimensions, Pressable, Linking, Animated} from 'react-native';
import { getUniqueId } from 'react-native-device-info';
import firestore from '@react-native-firebase/firestore';
import { useDispatch, useSelector } from 'react-redux';
import { HEADS, GOOGLE, SETTINGS } from '../images/index';
import { handleError } from '../helpers/errorHandling';
import RNPickerSelect from 'react-native-picker-select';
import RNRestart from 'react-native-restart';
import NetInfo from "@react-native-community/netinfo";
import CheckBox from '@react-native-community/checkbox';
import messaging from '@react-native-firebase/messaging';

import { GoogleSignin, GoogleSigninButton, statusCodes } from '@react-native-google-signin/google-signin';
import auth from '@react-native-firebase/auth';
const CURRENT_VERSION = 504;
const DEV_DB = true;

const Register = (props) => {
  const dispatch = useDispatch();
  dispatch({type: 'setVersion', values: CURRENT_VERSION});
  dispatch({type: 'setDev', values: DEV_DB});

  GoogleSignin.configure({
    webClientId: '460006790553-gs5lm7ctmruojao14dgi60maab40r461.apps.googleusercontent.com',
    offlineAccess: true,
    forceConsentPrompt: true,
  });

  const labels = [
    ['ඇතුල් වෙන්න', 'Continue', 'தொடரவும்'],
    ['කරුණාකර නම ඇතුලත් කරන්න', 'Please enter your name', 'உங்களுடைய பெயரை பதிவு செய்யவும்'],
    ['කරුණාකර ඔබගේ අන්තර්ජාල සම්බන්දතාව පරික්ෂා කර නැවත උත්සාහ කරන්න.','Please check your internet connection and try again', 'உங்கள் இணைய இணைப்பைச் சரிபார்த்து மீண்டும் முயற்சிக்கவும்'],
    ['නැවත උත්සාහ කරන්න', 'Try again', 'மீண்டும் முயற்சி செய்'],
    ['ඔබට වඩාත් හොඳ අත්දැකීමක් ලබාදීම සඳහා අදෘශ්‍යමාන යෙදුම යාවත්කාලින කර ඇත. කරුණාකර ඔබගේ යෙදුම යාවත්කාලින කරගන්න.', 'Adrushyamana has been updated to give you a better experience. Kindly update the app.', 'உங்களுக்கு சிறந்த அனுபவத்தை அளிக்க அத்ருஷ்யமனா மேம்படுத்தப்பட்டுள்ளது. தயவுசெய்து பயன்பாட்டைப் புதுப்பிக்கவும்.'],
    ['යාවත්කාලින කරන්න', 'Update', 'புதுப்பிக்கவும்'],
    ['රැඳී සිටින්න..', 'Please wait..', 'காத்திருக்கவும்..'],
    ['අන්තර්ගත භාෂාවන් :', 'Content languages :', 'உள்ளடக்க மொழிகள் :'],
    ['යෙදුම් භාෂාව : (App language)', 'App language :', 'பயன்பாட்டு மொழி :'],
    ['ඉහත සැකසුම්, සැකසුම් පිටුව හරහා පසුව වෙනස් කල හැක.', 'Above settings can be changed later from the settings screen.', 'மேலே உள்ள அமைப்புகளை பின்னர் அமைப்புகள் திரையில் இருந்து மாற்றலாம்.']
  ];

  let [userInfo, setUserInfo] = useState({});
  let [name, setName] = useState();
  let [language, setLanguage] = useState(0);
  let [nameError, setNameError] = useState(false);
  let [userDataFetched, setUserDataFetched] = useState(false);
  let [categoriesFetched, setCategoriesFetched] = useState(false);
  let [statsFetched, setStatsFetched] = useState(false);
  let [requestDetails, setRequestDetails] = useState(false);
  let [forceUpdateThreshold, setForceUpdateThreshold] = useState(0);
  let [latestTestVersion, setLatestTestVersion] = useState(0);
  let [forceUpdate, setForceUpdate] = useState(false);
  let [networkError, setNetworkError] = useState(false);
  let [signedInGoogle, setSignedInGoogle] = useState(false);
  let [buttonText, setButtonText] = useState(labels[0][language]);
  let [buttonTextIndex, setButtonTextIndex] = useState(0);
  let [active, setActive] = useState(true);
  let [categories, setCategories] = useState([]);
  let [userCategories, setUserCategories] = useState([]);

  let [progressText, setProgressText] = useState('...        ');
  let [progressIndex, setProgressIndex] = useState(0);

  const externalNetworkError = (props.route.params && props.route.params.networkError) || false;
  if(externalNetworkError) {
    active = false;
    // setActive(false);
    networkError = true;
    // setNetworkError(networkError);
  }

  let [sinhalaFilter, setSinhalaFilter] = useState(true);
  let [englishFilter, setEnglishFilter] = useState(true);
  let [tamilFilter, setTamilFilter] = useState(true);

  const dimensions = Dimensions.get('window');
  const imageHeight = Math.round(dimensions.width / 2.048);
  const screenWidth = dimensions.width;
  const signInButtonWidth = screenWidth*0.6;
  const signInButtonTextWidth = signInButtonWidth-60;

  const signIn = async () => {

    try {
      await GoogleSignin.hasPlayServices();
      userInfo = await GoogleSignin.signIn();
      const googleCredential = auth.GoogleAuthProvider.credential(userInfo.idToken);
      const googleToken = await auth().signInWithCredential(googleCredential);
      userInfo.user.uid = googleToken.user.uid;
      setUserInfo(userInfo);
      signedInGoogle = true;
      setSignedInGoogle(signedInGoogle);
      fetchData();
    } catch (err) {
      switch (err.code) {
        case '7': // network error
          active = false;
          setActive(active);
          networkError = true;
          setNetworkError(networkError);
          break;
        case '8': // internal error
          active = false;
          setActive(active);
          networkError = true;
          setNetworkError(networkError);
          handleError(err, 'register:signIn');
          break;
        default:
          active = false;
          setActive(active);
          networkError = true;
          setNetworkError(networkError);
          handleError(err, 'register:signIn');
          break;
      }
    }
  }

  const setFilter = (language, value) => {
    switch (language) {
      case 0:
        sinhalaFilter = value;
        setSinhalaFilter(sinhalaFilter);
        break;
      case 1:
        englishFilter = value;
        setEnglishFilter(englishFilter);
        break;
      case 2:
        tamilFilter = value;
        setTamilFilter(tamilFilter);
        break;
    }
  }

  React.useEffect(() => {
    const interval = setInterval(() => {
      if(active) {
        let string = ''
        for(let i=0;i<8;i++) {
          let j = progressIndex;
          if(progressIndex>7) {
            j = 15 - progressIndex;
          }
          if(i != j) {
            string += ' ';
          } else {
            string += '...';
          }
        }
        progressText = string;
        setProgressText(progressText);
        if(progressIndex < 15)
          progressIndex++;
        else
          progressIndex = 0;
        setProgressIndex(progressIndex);
      }
    }, 500);
    return () => clearInterval(interval);
  }, [])

  React.useEffect(async () => {
    if(externalNetworkError) return;
    const signedIn = await GoogleSignin.signInSilently().then(async (userInfoResponse) => {

      const getUser = await firestore().collection(DEV_DB? 'Users-dev': 'Users').doc(userInfoResponse.user.email).get().then(res => {
        if(res.data() && res.data().name) {
          if(res.data().pushNotifications) {
            messaging().setBackgroundMessageHandler(async remoteMessage => {});
            messaging().subscribeToTopic(
              (res.data.language == 0) ? 'Sinhala'
                : ((res.data.language == 1) ? 'English' : 'Tamil')
            );
          }
          if(CURRENT_VERSION%100 !== 0) {
            messaging().subscribeToTopic('Test');
          }
          name = res.data().name;
          setName(name);
          dispatch({type: 'setName', values: userInfoResponse.user.name});
          dispatch({type: 'setEmail', values: userInfoResponse.user.email});
          dispatch({type: 'setLanguage', values: res.data().language});
          dispatch({type: 'setLibrary', values: res.data().library});
          dispatch({type: 'setFavCategories', values: res.data().favCategories});
          dispatch({type: 'setContentLanguages', values: res.data().contentLanguages});
          dispatch({type: 'setPushNotifications', values: res.data().pushNotifications});
          userCategories = res.data().categories || [];
          setUserCategories(userCategories);
          dispatch({type: 'setUserCategories', values: userCategories});

          return true;
        } else {
          active = false;
          setActive(active);
          requestDetails = true;
          setRequestDetails(requestDetails);
          return false;
        }
      }).catch((err) => {
        if(err.toString().includes('firestore/permission-denied')) {
          active = false;
          setActive(active);
          requestDetails = true;
          setRequestDetails(requestDetails);
          return false;
        } else {
          handleError(err, 'register:getUserData')
        }
      });
      if(getUser) {
        userInfo = userInfoResponse;
        setUserInfo(userInfo);
        const googleCredential = auth.GoogleAuthProvider.credential(userInfoResponse.idToken);
        await auth().signInWithCredential(googleCredential);
      }
      return getUser;
    }).catch(err => {
      if(err.toString().includes('Failed to connect')) {
        active = false;
        setActive(active);
        networkError = true;
        setNetworkError(networkError);
        return;
      }
      switch (err.code) {
        case '4': // sign in cancelled (?)
          active = false;
          setActive(active);
          requestDetails = true;
          setRequestDetails(requestDetails);
          break;
        case '7': // network error
          active = false;
          setActive(active);
          networkError = true;
          setNetworkError(networkError);
          break;
        case '8': // internal error
          active = false;
          setActive(active);
          networkError = true;
          setNetworkError(networkError);
          handleError(err, 'register:silentSignIn');
          break;
        default:
          active = false;
          setActive(active);
          networkError = true;
          setNetworkError(networkError);
          handleError(err, 'register:silentSignIn');
          break;
        return false;
      }
    });

    if(!signedIn) return;
    else await fetchData();

    messaging().onNotificationOpenedApp(remoteMessage => {
      if(remoteMessage.data.documentId) {
        props.navigation.navigate(remoteMessage.data.screen, {documentId: remoteMessage.data.documentId, fromNotification: true});
      }
      if(remoteMessage.data.sub) {
        props.navigation.navigate(remoteMessage.data.screen, {sub: remoteMessage.data.sub, reload: true});
      }
    });

    // Check whether an initial notification is available
    const initialNotification = await new Promise(resolve => {
      messaging()
        .getInitialNotification()
        .then(remoteMessage => {
          if (remoteMessage) {
            if(remoteMessage.data.documentId) {
              props.navigation.navigate(remoteMessage.data.screen, {documentId: remoteMessage.data.documentId});
              resolve(true);
              return;
            }
            if(remoteMessage.data.sub) {
              props.navigation.navigate(remoteMessage.data.screen, {sub: remoteMessage.data.sub});
              resolve(true);
              return;
            }
            resolve(false);
            return;
          } else {
            resolve(false);
          }
        })
        .catch(() => {
          resolve(false);
        });
    })

    if(initialNotification) return;

    forceUpdate = (CURRENT_VERSION < forceUpdateThreshold)
      || ((CURRENT_VERSION%100 !== 0) && (CURRENT_VERSION < latestTestVersion));
    if(forceUpdate) {
      active = false;
      setActive(active);
    }
    setForceUpdate(forceUpdate);

    if(!networkError && !requestDetails && !forceUpdate) {
      active = false;
      setActive(active);
      props.navigation.navigate('Categories', {});
    }

  }, [])

  const fetchData = async () => {

    const categoriesPromise = firestore().collection(DEV_DB? 'Categories-dev': 'Categories').get().then((res) => {
      if(res.docs.length) {
        res.docs.sort((first, second) => {
          return first.data().documentId - second.data().documentId;
        });
        categories = res.docs.map(doc => doc.data());
        setCategories(categories);
        dispatch({ type: 'addCategories', values: categories })
      }
    }).catch(err => handleError(err, 'register:getCategories'));

    const statsPromise = firestore().collection(DEV_DB? 'Stats-dev': 'Stats').doc('0').get().then((res) => {
      if(res.data()) {
        dispatch({type: 'setStats', values: res.data()});
        dispatch({type: 'setContentMax', values: res.data()['0_lastIndex']});
        dispatch({type: 'setForceUpdateThreshold', values: res.data()['0_forceUpdateThreshold']});
        dispatch({type: 'setLatestProductionVersion', values: res.data()['0_latestProductionVersion']});
        dispatch({type: 'setLatestTestVersion', values: res.data()['0_latestTestVersion']});
        forceUpdateThreshold = res.data()['0_forceUpdateThreshold'];
        setForceUpdateThreshold(forceUpdateThreshold);
        latestTestVersion = res.data()['0_latestTestVersion'];
        setLatestTestVersion(latestTestVersion);
      }
    }).catch(err => handleError(err, 'register:getStats'));

    await categoriesPromise;

    const getLastVisited = (path) => {
      const pathSections = path.split(',');
      let firstIndex = -1;
      let secondIndex = -1;
      let thirdIndex = -1;
      for(const [i, section] of pathSections.entries()) {
        if(i == 0) {
          firstIndex = userCategories.findIndex(cat => (cat.name == section));
        }
        if((firstIndex != -1) && i == 1) {
          secondIndex = userCategories[firstIndex]
            .subCategories.findIndex(cat => (cat.name == section));
        }
        if((secondIndex != -1) &&i == 2) {
          thirdIndex = userCategories[firstIndex]
            .subCategories[secondIndex]
            .subCategories.findIndex(cat => (cat.name == section));
        }
      }
      let element = null;
      if(firstIndex > -1) {
        element = userCategories[firstIndex];
      }
      if(secondIndex > -1) {
        element = element.subCategories[secondIndex];
      }
      if(thirdIndex > -1) {
        element = element.subCategories[thirdIndex];
      }
      if(element) {
        return element.lastVisited || 0;
      }
      return 0;
    }

    const traverseArray = (array, path) => {
      for(const [i, cat] of array.entries()) {
        let catPath = path? path + ',' + cat.name : cat.name;
        cat.lastVisited = getLastVisited(catPath);
        if(cat.subCategories && cat.subCategories.length) {
          traverseArray(cat.subCategories, catPath);
        }
      }
    }

    traverseArray(categories, '');

    userCategories = categories;
    setUserCategories(userCategories);
    setCategories(categories);
    dispatch({ type: 'addCategories', values: categories });

    await firestore().collection(DEV_DB? 'Users-dev': 'Users').doc(userInfo.user.email).update({
      categories: userCategories
    }).catch((err) => handleError(err, 'register:fetchData'));
  };

  const registerUser = async () => {
    buttonTextIndex = 6;
    setButtonTextIndex(buttonTextIndex);

    messaging().setBackgroundMessageHandler(async remoteMessage => {});
    messaging().subscribeToTopic(
      (language == 0) ? 'Sinhala'
        : ((language == 1) ? 'English' : 'Tamil')
    );
    if(CURRENT_VERSION%100 !== 0) {
      messaging().subscribeToTopic('Test');
    }
    dispatch({type: 'setName', values: userInfo.user.name});
    dispatch({type: 'setEmail', values: userInfo.user.email});
    dispatch({type: 'setLanguage', values: language});
    dispatch({type: 'setContentLanguages', values: [sinhalaFilter, englishFilter, tamilFilter]});
    dispatch({type: 'setPushNotifications', values: true});
    const user = await firestore().collection(DEV_DB? 'Users-dev': 'Users').doc(userInfo.user.email).get().catch(() => {});
    if(!user) {
      await firestore().collection(DEV_DB? 'Users-dev': 'Users').doc(userInfo.user.email).set({
        name: userInfo.user.name,
        email: userInfo.user.email,
        uid: userInfo.user.uid,
        language: language,
        contentLanguages: [sinhalaFilter, englishFilter, tamilFilter],
        library: {},
        pushNotifications: true
      })
      .catch((err) => handleError(err, 'register:createUser'))
      .then(() => {
        props.navigation.navigate('Categories', {});
      })
    }

    messaging().onNotificationOpenedApp(remoteMessage => {
      if(remoteMessage.data.documentId) {
        props.navigation.navigate(remoteMessage.data.screen, {documentId: remoteMessage.data.documentId, fromNotification: true});
      }
      if(remoteMessage.data.sub) {
        props.navigation.navigate(remoteMessage.data.screen, {sub: remoteMessage.data.sub, reload: true});
      }
    });

    // Check whether an initial notification is available
    const initialNotification = await new Promise(resolve => {
      messaging()
        .getInitialNotification()
        .then(remoteMessage => {
          if (remoteMessage) {
            if(remoteMessage.data.documentId) {
              props.navigation.navigate(remoteMessage.data.screen, {documentId: remoteMessage.data.documentId});
              resolve(true);
              return;
            }
            if(remoteMessage.data.sub) {
              props.navigation.navigate(remoteMessage.data.screen, {sub: remoteMessage.data.sub});
              resolve(true);
              return;
            }
            resolve(false);
            return;
          } else {
            resolve(false);
          }
        })
        .catch(() => {
          resolve(false);
        });
    })

    if(initialNotification) return;

    props.navigation.navigate('Categories', {});
  };

  const reloadApp = () => {
    RNRestart.Restart();
  }

  const updateApp = () => {
    Linking.openURL('https://play.google.com/store/apps/details?id=com.adrushyamana.adrushyamanaPasala');
  }

  let template = (
    <View style={{
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center'
    }}>
      <Text style={{color: 'white', fontSize: 17}}>
        {progressText}
      </Text>
    </View>
  );

  let networkErrorTemplate = (
    <View>
      <Text style={styles.text}>{labels[2][language]}</Text>
      <Pressable
        onPress={reloadApp}>
        <View style={styles.button}>
          <Text style={styles.text}>
            {labels[3][language]}
          </Text>
        </View>
      </Pressable>
    </View>
  )

  let forceUpdateTemplate = (
    <View>
      <Text style={styles.text}>{labels[4][language]}</Text>
      <Pressable
        onPress={updateApp}>
        <View style={styles.button}>
          <Text style={styles.text}>
            {labels[5][language]}
          </Text>
        </View>
      </Pressable>
    </View>
  )

  let inputDetails = (
    <View>
    {!signedInGoogle &&
      <Pressable
        onPress={signIn}>
        <View style={styles.googleSignIn}>
          <View style={{...styles.googleSignInButton, width: signInButtonWidth}}>
            <Image
              style={{ height: 30, width: 30, margin: 0, padding: 0}}
              source={GOOGLE}/>
            <View style={{...styles.googleSignInTextContainer, width: signInButtonTextWidth}}>
              <Text style={styles.googleSignInText}>Sign in with Google</Text>
            </View>
          </View>
        </View>
      </Pressable>
    }
    {signedInGoogle &&
      <View>
        <View style={styles.setting}>
          <Text style={styles.settingsText}>
            {labels[8][language]}
          </Text>
          <View style={{backgroundColor: '#67c78d'}}>
            <RNPickerSelect
              onValueChange={setLanguage}
              placeholder={{}}
              items={[
                  { label: 'සිංහල', value: 0 },
                  { label: 'English', value: 1 },
                  { label: 'தமிழ்', value: 2 },
              ]}/>
          </View>
        </View>
        <View style={styles.setting}>
          <Text style={styles.settingsText}>{labels[7][language]}</Text>
          <View style={styles.checkboxContainer}>
            <CheckBox
              value={sinhalaFilter}
              onValueChange={(newValue) => setFilter(0, newValue)}
            />
            <Text style={styles.checkboxesText}>සිංහල</Text>
          </View>
          <View style={styles.checkboxContainer}>
            <CheckBox
              value={englishFilter}
              onValueChange={(newValue) => setFilter(1, newValue)}
            />
            <Text style={styles.checkboxesText}>English</Text>
          </View>
          <View style={styles.checkboxContainer}>
            <CheckBox
              value={tamilFilter}
              onValueChange={(newValue) => setFilter(2, newValue)}
            />
            <Text style={styles.checkboxesText}>தமிழ்</Text>
          </View>
        </View>
        <View style={styles.setting}>
          <View style={{...styles.settingsTip}}>
            <View style={styles.settingsButton}>
              <Image
                style={styles.settingsImage}
                source={SETTINGS}/>
            </View>
            <Text style={styles.tipText}>{labels[9][language]}</Text>
          </View>
        </View>
        <Pressable
          onPress={registerUser}>
          <View style={{...styles.button, width: screenWidth}}>
            <Text style={styles.buttonText}>
              {labels[buttonTextIndex][language]}
            </Text>
          </View>
        </Pressable>
      </View>
    }
    </View>
  )

  if(forceUpdate) {
    template = forceUpdateTemplate;
  } else if(networkError) {
    template = networkErrorTemplate;
  } else if(requestDetails) {
    template = inputDetails;
  }

  return (
    <View style={styles.mainContainer}>
      <Image
        style={{ height: imageHeight, width: screenWidth}}
        source={HEADS}/>
      {template}
    </View>
  );
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    height: '100%',
    width: '100%',
    backgroundColor: '#00593a',
    alignItems: 'center',
    justifyContent: 'center'
  },
  text: {
    fontSize: 16,
    color: 'white',
    marginLeft: 10
  },
  textError: {
    fontSize: 16,
    color: '#22b07f',
    marginLeft: 10,
    marginBottom: 10
  },
  textInput: {
    backgroundColor: 'white',
    borderRadius: 5,
    margin: 10
  },
  button: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(255, 255, 255, 0.2)',
    height: 60,
    padding: 15,
    marginTop: 20,
    marginBottom: 10,
    borderRadius: 7,
    fontSize: 16
  },
  inputAndroid: {
    color: 'white'
  },
  googleSignIn: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  googleSignInButton: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: 'white',
    borderRadius: 10,
    overflow: 'hidden',
    padding: 15,
    backgroundColor: '#164227',
    borderWidth: 1,
    borderColor: 'black'
  },
  googleSignInTextContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    margin: 0,
    padding: 0
  },
  googleSignInText: {
    fontSize: 14,
    fontFamily: 'sans-serif-medium',
    color: 'white'
  },
  settingsText: {
    color: 'white',
    padding: 10,
    fontSize: 15
  },
  checkboxContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginLeft: 10,
    marginBottom: 5,
  },
  checkboxesText: {
    color: 'white',
    marginLeft: 10,
  },
  setting: {
    marginBottom: 10
  },
  settingsButton: {
    width: 35,
    height: 35,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#5bb993',
    borderRadius: 7,
    borderWidth: 1,
    borderColor: '#4b8c72'
  },
  settingsImage: {
    height: 20,
    width: 20,
    padding: 7.5
  },
  settingsTip: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    padding: 10,
    width: '100%'
  },
  tipText: {
    color: 'white',
    marginLeft: 10,
    width: '80%',
    fontSize: 12
  },
  buttonText: {
    color: 'white',
    marginLeft: 10,
    fontSize: 15
  },
});


export default Register;
