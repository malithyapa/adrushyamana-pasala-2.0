import React, { useState, useCallback, useRef } from "react";
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  ScrollView,
  Pressable,
  Image,
  BackHandler,
  useWindowDimensions
} from 'react-native';
import YoutubePlayer from "react-native-youtube-iframe";
import firestore from '@react-native-firebase/firestore';
import { useSelector, useDispatch } from "react-redux";
import AnimatedLoader from "react-native-animated-loader";
import { LIBRARY_B, PLAY, BACK } from '../images/index';
import { getUniqueId } from 'react-native-device-info';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import Toast from 'react-native-simple-toast';
import { handleError } from '../helpers/errorHandling';
import { useFocusEffect } from '@react-navigation/native';

const Video = (props) => {

  let [category, setCategory] = useState(null);

  useFocusEffect(() => {
    const backAction = () => {
      if(props.route.params.fromSearch)
        props.navigation.navigate('Search', {});
      else if (props.route.params.fromLibrary)
        props.navigation.navigate('Library', {});
      else if(category)
        props.navigation.navigate('Content', {sub: category, reload: props.route.params.fromNotification});
      else
        props.navigation.navigate('Categories', {});
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      backAction
    );

    return () => backHandler.remove();
  });

  const dispatch = useDispatch();
  const email = useSelector(state => state.email);

  let [videoData, setVideoData] = useState(null);
  const [playerReady, setPlayerReady] = useState(false);
  const scrollView = useRef();

  let language = useSelector(state => state.language);
  const DEV_DB = useSelector(state => state.dev);
  const documentsInLibrary = useSelector(state => state.documentsInLibrary);
  let library = useSelector((state) => state.library);

  let notes = [];
  if(library['සටහන් (Notes)'] && videoData && library['සටහන් (Notes)'][videoData.documentId]) {
    notes = library['සටහන් (Notes)'][videoData.documentId].notes;
  }

  const labels = [
    ['පුස්තකාලයට දමන්න ', 'Add to Library', 'நூலகத்தில் சேர்க்கவும்'],
    ['පුස්තකාලයෙන් ඉවත් කරන්න', 'Remove from Library', 'நூலகத்திலிருந்து அகற்று'],
    ['අදහස්', 'Comments', 'கருத்துகள்'],
    ['සටහන්', 'Notes', 'குறிப்புகள்'],
    ['අදහසක් දක්වන්න:', 'Add comment:', 'கருத்தைச் சேர்க்கவும்'],
    ['සටහනක් ඇතුල් කරන්න:', 'Add note:', 'குறிப்பு சேர்க்க'],
    ['අදහස් ඇතුල් කරන ලදි', 'Comment submitted', 'கருத்து சமர்ப்பிக்கப்பட்டது'],
    ['සටහන ඇතුල් කරන ලදිි', 'Note submitted', 'குறிப்பு சமர்ப்பிக்கப்பட்டது'],
    ['පුස්තකාලයට දමන ලදීි', 'Added to library', 'நூலகத்தில் சேர்க்கப்பட்டது'],
    ['පුස්තකාලයෙන් ඉවත් කරන ලදී', 'Removed from library', 'நூலகத்திலிருந்து அகற்றப்பட்டது'],
    ['මෙහි දක්වන අදහස් සියල්ලන්ටම දිස් වේ.', 'Comments added here will be visible to everyone', 'இங்கு சேர்க்கப்படும் கருத்துகள் அனைவருக்கும் தெரியும்'],
    ['මෙහි දක්වන සටහන් දිස් වන්නේ ඔබට පමණි.', 'Notes added here will only be visible to you', 'இங்கே சேர்க்கப்பட்ட குறிப்புகள் உங்களுக்கு மட்டுமே தெரியும்'],
    ['ඊළඟ කොටස ', 'Next Video', 'அடுத்த வீடியோ'],
    ['රැඳී සිටින්න..', 'Please wait..', 'காத்திருக்கவும்..'],
  ]

  const layout = useWindowDimensions();

  const [index, setIndex] = React.useState(0);

  const videoId = props.route.params.documentId;

  let [mounted, setMounted] = useState(true);

  let [disableAddToLibrary, setDisableAddToLibrary] = useState(false);

  useFocusEffect(
    React.useCallback(() => {
      firestore().collection(DEV_DB? 'Content-dev': 'Content').doc(videoId).get().then((response) => {
        videoData = {
          ...response.data()
        };
        category = response.data().categories[0];
        setCategory(category);
        firestore().collection(DEV_DB? 'Comments-dev': 'Comments').doc(videoId).get().then((response) => {
          if(response.data()) {
            videoData.comments = response.data().comments;
          } else {
            videoData.comments = [];
          }
          setVideoData(videoData);
        }).catch(err => {
          throw 'Couldnt fetch data' + err;
        });
      }).catch(err => {
        throw 'Couldnt fetch data' + err;
      });
    }, [videoId])
  );

  React.useEffect(() => {
    return () => {
      setMounted(false);
    }
  }, [])

  const [comment, setComment] = useState();
  const name = useSelector(state => state.name);

  const submitComment = (event) => {
    Toast.show(labels[6][language]);
    const comment = event.nativeEvent.text;
    const comments = videoData.comments;
    comments.push({name, comment})
    firestore().collection(DEV_DB? 'Comments-dev': 'Comments').doc(videoData.documentId).set({
      comments
    }).catch((err) => handleError(err, 'video:submitComment'));
    setVideoData({...videoData});
  }

  const submitNote = (event) => {
    Toast.show(labels[7][language]);
    const timestamp = new Date();
    const note =  {
      note: event.nativeEvent.text,
      time: '' + (timestamp.getMonth() + 1) + '/' + timestamp.getDate() + '  ' + timestamp.getHours() + ':' + timestamp.getMinutes()
    }

    if(!library['සටහන් (Notes)']) {
      library['සටහන් (Notes)'] = {};
    }
    if(!library['සටහන් (Notes)'][videoData.documentId]) {
      library['සටහන් (Notes)'][videoData.documentId] = {
        title: videoData.title,
        notes: [note],
        video: videoData
      };
    } else {
      library['සටහන් (Notes)'][videoData.documentId].notes.push(note);
    }
    dispatch({type: 'setLibrary', values: library});

    firestore().collection(DEV_DB? 'Users-dev': 'Users')
      .doc(email)
      .update({library})
      .catch(err => handleError(err, 'video:submitNote'));

    // videoData.comments.push({name, comment});
    setVideoData({...videoData});

  }

  const comments = [];
  if(videoData && videoData.comments && videoData.comments.length) {
    videoData.comments.map((comment, index) => {
      comments.push(
        <View key={index} style={styles.commentContainer}>
          <Text style={styles.commentOwner}>{comment.name}</Text>
          <Text style={styles.commentContent}>{comment.comment}</Text>
        </View>
      );
    });
  } else {
    comments.push(
      <View key={index} style={styles.commentContainer}>
        <Text style={styles.commentContent}>{labels[10][language]}</Text>
      </View>
    );
  }

  const notesTemplate = []
  if(notes && notes.length) {
    notes.map((note, index) => {
      notesTemplate.push(
        <View key={index} style={styles.commentContainer}>
          <Text style={styles.commentOwner}>{note.time}</Text>
          <Text style={styles.commentContent}> - {note.note}</Text>
        </View>
      );
    });
  } else {
    notesTemplate.push(
      <View key={index} style={styles.commentContainer}>
        <Text style={styles.commentContent}>{labels[11][language]}</Text>
      </View>
    );
  }

  const [routes] = React.useState([
    { key: 'first', title: labels[2][language] },
    { key: 'second', title: labels[3][language] },
  ]);

  const renderTabBar = props => (
    <TabBar
      {...props}
      indicatorStyle={{ backgroundColor: 'white' }}
      style={{ backgroundColor: 'rgba(255, 255, 255, 0.2)', color: 'black' }}
    />
  );

  const FirstRoute = () => (
    <View style={{  backgroundColor: '#00593a' }}>
      <Text style={styles.commentText}>{labels[4][language]}</Text>
      <TextInput
        style={styles.searchText}
        color={'black'}
        placeholderTextColor={'black'}
        onSubmitEditing={submitComment}/>
      {comments}
    </View>
  );

  const SecondRoute = () => (
    <View style={{  backgroundColor: '#00593a' }}>
      <Text style={styles.commentText}>{labels[5][language]}</Text>
      <TextInput
        style={styles.searchText}
        color={'black'}
        placeholderTextColor={'black'}
        onSubmitEditing={submitNote}/>
      {notesTemplate}
    </View>
  );

  const renderScene = SceneMap({
    first: FirstRoute,
    second: SecondRoute,
  });

  let template = <View style={styles.mainContainer}></View>;

  const addToLibrary = (video) => {
    disableAddToLibrary = true;
    setDisableAddToLibrary(disableAddToLibrary);
    Toast.show(labels[8][language]);
    video.categories.forEach((cat) => {
      if(!library[cat]) {
        library[cat] = [];
      }
      library[cat].push(video);
    });

    dispatch({type: 'setLibrary', values: library});

    firestore().collection(DEV_DB? 'Users-dev': 'Users')
      .doc(email)
      .update({library})
      .then(() => {
        disableAddToLibrary = false;
        setDisableAddToLibrary(disableAddToLibrary);
      })
      .catch(err => handleError(err, 'video:addToLibrary'));
  }

  const removeFromLibrary = (video) => {
    disableAddToLibrary = true;
    setDisableAddToLibrary(disableAddToLibrary);
    Toast.show(labels[9][language]);
    video.categories.forEach((cat) => {
      library[cat] = library[cat].filter(vid => {
        return vid.documentId !== video.documentId;
      })
    });
    dispatch({type: 'setLibrary', values: library});

    firestore().collection(DEV_DB? 'Users-dev': 'Users')
      .doc(email)
      .update({library})
      .then(() => {
        disableAddToLibrary = false;
        setDisableAddToLibrary(disableAddToLibrary);
      })
      .catch(err => handleError(err, 'video:removeFromLibrary'));
  }

  const gotoNextVideo = () => {
    setVideoId(videoData.nextDocument);
    scrollView.current.scrollTo({
      y: 0,
      animated: true,
    });
  }

  if(videoData) {
    let languageTag = 'සි';
    if(videoData.language.localeCompare('English') === 0) {
      languageTag = 'En';
    }
    if(videoData.language.localeCompare('Tamil') === 0) {
      languageTag = 'த';
    }
    template = (
      <View style={styles.mainContainer}>
        <View style={styles.screenHeader}>
          <Pressable
            onPress={() => {
              if(props.route.params.fromSearch)
                props.navigation.navigate('Search', {});
              else if (props.route.params.fromLibrary)
                props.navigation.navigate('Library', {});
              else if(category)
                props.navigation.navigate('Content', {sub: category, reload: props.route.params.fromNotification});
              else
                props.navigation.navigate('Categories', {});
            }}>
            <View style={styles.backButtonTouchArea}>
              <Image
                style={{height: 25, width: 25}}
                source={BACK}/>
            </View>
          </Pressable>
          <View style={styles.screenName}>
            <Text style={styles.screenNameText}>වීඩියෝව (Video)</Text>
          </View>
        </View>
        <YoutubePlayer
            style={styles.videoElement}
            height={240}
            play={false}
            videoId={videoData.videoId}
            onReady={() => {
              setPlayerReady(true)
            }}
          />
        <Text style={styles.videoTitle}> {videoData.title} </Text>
        <View style={styles.detailsBar}>
          <Text style={styles.channelName}>{videoData.channel}</Text>
          <Text style={styles.language}>{languageTag}</Text>
        </View>
        {(videoData.groupStatus != 0) && (language == 0) && (
          <View style={styles.courseTag}>
            <Text style={styles.courseTagText}>* වීඩියෝ {videoData.groupCount} සහිත පාඨමාලාවකි.</Text>
          </View>
        )}
        {(videoData.groupStatus != 0) && (language == 1) && (
          <View style={styles.courseTag}>
            <Text style={styles.courseTagText}>* This is a course with {videoData.groupCount} videos.</Text>
          </View>
        )}
        {(videoData.groupStatus != 0) && (language == 2) && (
          <View style={styles.courseTag}>
            <Text style={styles.courseTagText}>* இது {videoData.groupCount} வீடியோக்கள் கொண்ட பாடநெறி.</Text>
          </View>
        )}
        <Text style={styles.shortDescription}>
          {videoData.description}
        </Text>
        <Pressable
          onPress={() => {
            if(documentsInLibrary.has(videoData.documentId)) {
              removeFromLibrary(videoData);
            } else {
              addToLibrary(videoData);
            }
          }}>
            <View style={{...styles.libraryButton, backgroundColor: disableAddToLibrary ? '#5a9e83' : '#549078'}}>
              <Image
                style={{ height: 25, width: 25}}
                source={LIBRARY_B}/>
              <Text style={styles.libraryButtonText}>
                {documentsInLibrary.has(videoData.documentId) ? labels[1][language] : labels[0][language]}
              </Text>
            </View>
        </Pressable>
        {videoData.nextDocument && (
          <Pressable
            onPress={() => gotoNextVideo()}>
            <View style={styles.libraryButton}>
              <Image
                style={{ height: 25, width: 25}}
                source={PLAY}/>
              <Text style={styles.libraryButtonText}>
              {labels[12][language]}
              </Text>
            </View>
          </Pressable>
        )}
        <View style={{height: ((comments.length *100) > 400) ? (comments.length *100) : 400}}>
          <TabView
            renderTabBar={renderTabBar}
            style={{backgroundColor: '#00593a'}}
            navigationState={{ index, routes }}
            renderScene={renderScene}
            onIndexChange={setIndex}
            initialLayout={{ width: layout.width }}
          />
        </View>

      </View>
    )
  }
  return (
    <View style={styles.mainContainer}>
      <ScrollView ref={scrollView}>
        {template}
      </ScrollView>
      <AnimatedLoader
        visible={!(playerReady && videoData)}
        overlayColor="#00593a"
        source={require("./9379-loader.json")}
        animationStyle={styles.lottie}
        speed={1}>
      </AnimatedLoader>
    </View>
  )
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: '#00593a'
  },
  libraryButton: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#549078',
    height: 70,
    padding: 15,
    marginBottom: 7,
    borderRadius: 7,
    width: "100%"
  },
  libraryButtonText: {
    marginLeft: 10,
    fontSize: 15,
  },
  searchText: {
    backgroundColor: 'white'
  },
  videoTitle: {
    fontSize: 19,
    color: 'white'
  },
  shortDescription: {
    paddingLeft: 5,
    marginBottom: 10,
    fontSize: 15,
    color: 'white'
  },
  videoElement: {
    marginBottom: 10,
    paddingBottom: 0,
    maxHeight: 180
  },
  commentText: {
    color: '#22b07f',
    fontSize: 15,
    marginTop: 5,
    padding: 5
  },
  commentContainer: {
    borderTopColor: 'grey',
    borderTopWidth: 1,
    borderBottomColor: 'grey',
    borderBottomWidth: 1,
    padding: 10,
    paddingLeft: 5
  },
  commentOwner: {
    color: '#22b07f',
    fontSize: 12,
    marginBottom: 5
  },
  commentContent: {
    color: 'white'
  },
  lottie: {
    width: 80,
    height: 80
  },
  detailsBar: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 5,
    paddingRight: 0
  },
  channelName: {
    fontSize: 12,
    color: '#22b07f'
  },
  language: {
    fontSize: 12,
    color: 'white',
    borderColor: '#22b07f',
    borderWidth: 1,
    padding: 3,
    paddingLeft: 5
  },
  courseTag: {
    padding: 3
  },
  courseTagText: {
    color: '#c1c1c1'
  },
  screenHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    padding: 0,
    margin: 0,
    height: 80,
    backgroundColor: '#194437'
  },
  backButtonTouchArea: {
    width: '10%',
    padding: 15,
  },
  screenName: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  screenNameText: {
    marginLeft: 15,
    fontSize: 17,
    fontWeight: "600",
    color: 'white'
  },
});

export default Video;
