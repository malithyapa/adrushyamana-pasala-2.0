import React, { useState, useCallback, useRef } from "react";
import { StyleSheet, Text, View, ScrollView, TextInput, Image, Dimensions, Pressable} from 'react-native';
import { BACK } from '../images/index';
import { useDispatch, useSelector } from 'react-redux';
import CheckBox from '@react-native-community/checkbox';
import RNPickerSelect from 'react-native-picker-select';
import firestore from '@react-native-firebase/firestore';
import messaging from '@react-native-firebase/messaging';

String.prototype.hashCode = function() {
  var hash = 0, i, chr;
  if (this.length === 0) return hash;
  for (i = 0; i < this.length; i++) {
    chr   = this.charCodeAt(i);
    hash  = ((hash << 5) - hash) + chr;
    hash |= 0;
  }
  return hash;
};

const getRGBFromString = (string) => {
  const hash = string.hashCode();
  const r = (hash & 0xFF0000) >> 16;
  const g = (hash & 0x00FF00) >> 8;
  const b = hash & 0x0000FF;
  return `rgba(${r}, ${g}, ${b}, 0.2)`
}

const Settings = (props) => {

  dispatch = useDispatch();

  const contentLanguages = useSelector(state => state.contentLanguages);
  let [language, setLanguage] = useState(useSelector(state => state.language));
  const DEV_DB = useSelector(state => state.dev);
  const email = useSelector(state => state.email);
  let [sinhalaFilter, setSinhalaFilter] = useState(contentLanguages[0]);
  let [englishFilter, setEnglishFilter] = useState(contentLanguages[1]);
  let [tamilFilter, setTamilFilter] = useState(contentLanguages[2]);

  const labels = [
    ['යෙදුම් භාෂාව :', 'App language :', 'பயன்பாட்டு மொழி :'],
    ['අන්තර්ගත භාෂාවන් :', 'Content languages :', 'உள்ளடக்க மொழிகள் :'],
    ['දැනුම්දීම් :', 'Notifications :', 'அறிவிப்புகள் :'],
    ['දැනුම්දීම් අක්‍රිය කරන්න', 'Disable push notifications', 'அறிவிப்புகளை முடக்கு'],
    ['දැනුම්දීම් සක්‍රීය කරන්න', 'Enable push notifications', 'அறிவிப்புகளை இயக்கவும்'],
    ['යෙදුම :', 'Updates :', 'புதுப்பிப்புகள் :'],
    ['යෙදුම යාවත්කාලීන කරන්න', 'Update app', 'புதுப்பி'],
  ];

  const updateAvailable = useSelector(state => state.version) < useSelector(state => state.latestProductionVersion);

  let version = useSelector(state => state.version);

  let [notifications, setNotifications] = useState(useSelector(state => state.pushNotifications))
  const dimensions = Dimensions.get('window');
  const imageHeight = Math.round(dimensions.width / 2.048);
  const screenWidth = dimensions.width;
  const buttonTextWidth = screenWidth-135;

  const setAppLanguage = (val) => {
    messaging().unsubscribeFromTopic((language == 0) ? 'Sinhala' : ((language == 1) ? 'English' : 'Tamil'));
    language = val;
    setLanguage(language);
    messaging().subscribeToTopic((language == 0) ? 'Sinhala' : ((language == 1) ? 'English' : 'Tamil'));

    dispatch({type: 'setLanguage', values: language});
    firestore().collection(DEV_DB? 'Users-dev': 'Users').doc(email).update({
      language: language
    })
    .catch((err) => handleError(err, 'settings:setAppLanguage'));
  }

  const setFilter = (language, value) => {
    switch (language) {
      case 0:
        sinhalaFilter = value;
        setSinhalaFilter(sinhalaFilter);
        break;
      case 1:
        englishFilter = value;
        setEnglishFilter(englishFilter);
        break;
      case 2:
        tamilFilter = value;
        setTamilFilter(tamilFilter);
        break;
    }
    dispatch({type: 'setContentLanguages', values: [sinhalaFilter, englishFilter, tamilFilter]});
    firestore().collection(DEV_DB? 'Users-dev': 'Users').doc(email).update({
      contentLanguages: [sinhalaFilter, englishFilter, tamilFilter]
    })
    .catch((err) => handleError(err, 'settings:setFilter'));
  }

  const toggleNotifications = () => {
    notifications = !notifications;
    setNotifications(notifications);
    dispatch({type: 'setNotifications', values: notifications});
    firestore().collection(DEV_DB? 'Users-dev': 'Users').doc(email).update({
      pushNotifications: notifications
    })
    .catch((err) => handleError(err, 'setiings:toggleNotifications'));
    if(!notifications) {
      messaging().unsubscribeFromTopic((language == 0) ? 'Sinhala' : ((language == 1) ? 'English' : 'Tamil'));
    } else {
      messaging().subscribeToTopic((language == 0) ? 'Sinhala' : ((language == 1) ? 'English' : 'Tamil'));
    }
  }

  return (
    <ScrollView style={styles.mainContainer}>
      <View style={styles.screenHeader}>
        <Pressable
          onPress={() => props.navigation.navigate('Categories', {})}>
          <View style={styles.backButtonTouchArea}>
            <Image
              style={{height: 25, width: 25}}
              source={BACK}/>
          </View>
        </Pressable>
        <View style={styles.screenName}>
          <Text style={styles.screenNameText}>සැකසුම් (Settings)</Text>
        </View>
      </View>
      <View style={styles.setting}>
        <Text style={styles.settingsText}>{labels[0][language]}</Text>
        <View style={{backgroundColor: '#9e9e9e'}}>
          <RNPickerSelect
            onValueChange={(val) => setAppLanguage(val)}
            value={language}
            placeholder={{}}
            items={[
                { label: 'සිංහල', value: 0 },
                { label: 'English', value: 1 },
                { label: 'தமிழ்', value: 2 },
            ]}/>
        </View>
      </View>
      <View style={styles.setting}>
        <Text style={styles.settingsText}>{labels[1][language]}</Text>
        <View style={styles.checkboxContainer}>
          <CheckBox
            value={sinhalaFilter}
            onValueChange={(newValue) => setFilter(0, newValue)}
          />
          <Text style={styles.checkboxesText}>සිංහල</Text>
        </View>
        <View style={styles.checkboxContainer}>
          <CheckBox
            value={englishFilter}
            onValueChange={(newValue) => setFilter(1, newValue)}
          />
          <Text style={styles.checkboxesText}>English</Text>
        </View>
        <View style={styles.checkboxContainer}>
          <CheckBox
            value={tamilFilter}
            onValueChange={(newValue) => setFilter(2, newValue)}
          />
          <Text style={styles.checkboxesText}>தமிழ்</Text>
        </View>
      </View>
      <View style={styles.setting}>
        <Text style={styles.settingsText}>{labels[2][language]}</Text>
        <Pressable
          onPress={() => toggleNotifications()}>
          <View style={{...styles.notificationButton}}>
            <Text style={styles.buttonText}>{notifications ? labels[3][language] : labels[4][language]}</Text>
          </View>
        </Pressable>
      </View>
      <View style={styles.setting}>
        <Text style={styles.settingsText}>{labels[5][language]}</Text>
        <Pressable
          onPress={() => updateApp()}
          disabled={!updateAvailable}>
          <View style={{...styles.notificationButton, backgroundColor: updateAvailable ? '#888787' : '#3a3a3a'}}>
            <Text style={styles.buttonText}>{labels[6][language]}</Text>
          </View>
        </Pressable>
      </View>
      <View>
        <Text style={{marginTop: 10, padding: 10, color: 'white', fontSize: 12}}>Version: {(version/100).toFixed(2)} {(version%100 !== 0) ? '(Internal Test Version)' : ''} {DEV_DB? '(Connected to Dev database)': ''}</Text>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  mainContainer: {
    flexGrow: 1,
    backgroundColor: '#00593a'
  },
  screenHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    padding: 0,
    margin: 0,
    height: 80,
    backgroundColor: '#194437'
  },
  backButtonTouchArea: {
    width: '10%',
    padding: 15,
  },
  screenName: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  screenNameText: {
    marginLeft: 15,
    fontSize: 17,
    fontWeight: "600",
    color: 'white'
  },
  settingsText: {
    color: 'white',
    padding: 10
  },
  checkboxContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginLeft: 10,
    marginBottom: 5,
  },
  checkboxesText: {
    color: 'white',
    marginLeft: 10
  },
  picker: {
    backgroundColor: '#9e9e9e'
  },
  setting: {
    marginBottom: 10
  },
  notificationButton: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#888787',
    padding: 20,
    fontSize: 17
  },
  buttonText: {
    fontSize: 15
  },
  inputAndroid: {
    fontSize: 15
  }
});

export default Settings;
