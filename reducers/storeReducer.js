export default function storeReducer(state = {
  categories: [],
  deviceId: null,
  userCategories: [],
  favCategories: [],
  documentsInLibrary: new Set(),
  stats: {},
  name: null,
  language: 0,
  contentLanguages: [true, true, true],
  email: null,
  contentMax: 0,
  forceUpdateThreshold: 1000,
  latestProductionVersion: 0,
  latestTestVersion: 0,
  customMessageShown: false,
  version: null,
  pushNotifications: true,
  dev: false
}, action)
{
  switch (action.type) {
    case 'addCategories':
      state.categories = action.values;
      break;
    case 'setDeviceId':
      state.deviceId = action.values;
      break;
    case 'setLibrary':
      state.documentsInLibrary = new Set();
      Object.keys(action.values)
        .filter(category => category != 'සටහන් (Notes)')
        .map(category => {
          action.values[category].map(document => {
            state.documentsInLibrary.add(document.documentId);
          });
        });
      state.library = action.values;
      break;
    case 'setName':
      state.name = action.values;
      break;
    case 'setEmail':
      state.email = action.values;
      break;
    case 'setLanguage':
      state.language = action.values;
      break;
    case 'setStats':
      state.stats = action.values;
      break;
    case 'setFavCategories':
      state.favCategories = action.values;
      break;
    case 'setStats':
      state.stats = action.values;
      break;
    case 'setContentMax':
      state.contentMax = action.values;
      break;
    case 'setForceUpdateThreshold':
      state.forceUpdateThreshold = action.values;
      break;
    case 'setCustomMessageShown':
      state.customMessageShown = action.values;
      break;
    case 'setVersion':
      state.version = action.values;
      break;
    case 'setPushNotifications':
      state.pushNotifications = action.values;
      break;
    case 'setLatestProductionVersion':
      state.latestProductionVersion = action.values;
      break;
    case 'setLatestTestVersion':
      state.latestTestVersion = action.values;
      break;
    case 'setContentLanguages':
      state.contentLanguages = action.values;
      break;
    case 'setDev':
      state.dev = action.values;
      break;
    case 'setUserCategories':
      state.userCategories = action.values;
      break;
  }
  return state;
}
