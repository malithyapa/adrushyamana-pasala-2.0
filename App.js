/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type {Node} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import firestore from '@react-native-firebase/firestore';

import Categories from './components/categories';
import Content from './components/content';
import Search from './components/search';
import Library from './components/library';
import About from './components/about';
import Register from './components/register';
import Video from './components/video';
import Webview from './components/webview';
import Settings from './components/settings';

import SplashScreen from 'react-native-splash-screen';
import store from './helpers/store'
import { Provider } from 'react-redux'


import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
const Stack = createStackNavigator();

import { getUniqueId } from 'react-native-device-info';
import YoutubePlayer from "react-native-youtube-iframe";

import NetInfo from "@react-native-community/netinfo";
import AsyncStorage from '@react-native-async-storage/async-storage';
import messaging from '@react-native-firebase/messaging';

const App: () => Node = () => {


  const updateErrors = async () => {
    const errors = JSON.parse(await AsyncStorage.getItem('@errors')) || [];
    const localErrors = []
    for(let error of errors) {
      await firestore().collection('Errors').add(error).catch(async (err) => {
        localErrors.push(error)
      });
    }
    await AsyncStorage.setItem('@errors', JSON.stringify(localErrors))
  }

  updateErrors();

  const playerLoaded = () => {
    SplashScreen.hide();
  }

  setTimeout(() => {
    NetInfo.addEventListener(state => {
      if(!state.isInternetReachable) {
        SplashScreen.hide();
      }
    });
  }, 5000);

  return (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen
            name="Register"
            component={Register}
            options={{
              animationEnabled: false,
              headerShown: false
            }}/>
          <Stack.Screen
            name="Categories"
            component={Categories}
            options={{
              animationEnabled: false,
              headerShown: false
            }}/>
          <Stack.Screen
            name="Content"
            component={Content}
            options={{
              headerShown: false,
              animationEnabled: false
            }}/>
          <Stack.Screen
            name="Search"
            component={Search}
            options={{
              animationEnabled: false,
              headerShown: false
            }}/>
          <Stack.Screen
            name="About"
            component={About}
            options={{
              animationEnabled: false,
              headerShown: false
            }}/>
          <Stack.Screen
            name="Library"
            component={Library}
            options={{
              animationEnabled: false,
              headerShown: false
            }}/>
          <Stack.Screen
            name="WebView"
            component={Webview}
            options={{
              animationEnabled: false,
              headerShown: false
            }}/>
          <Stack.Screen
            name="Video"
            component={Video}
            options={{
              animationEnabled: false,
              headerShown: false
            }}/>
          <Stack.Screen
            name="Settings"
            component={Settings}
            options={{
              animationEnabled: false,
              headerShown: false
            }}/>
        </Stack.Navigator>
      </NavigationContainer>
      <View style={{height: 0, opacity: 0}}>
        <YoutubePlayer
              height={270}
              play={false}
              videoId={'klg-ssXMaLM'}
              onChangeState={() => {}}
              onReady={playerLoaded}
            />
      </View>
    </Provider>
  );
};

const styles = StyleSheet.create({
  // NA
});

export default App;
